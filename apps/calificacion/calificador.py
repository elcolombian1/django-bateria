# -*- coding: utf-8 -*-
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from respuestas_diccionario import *
import os
FormaA=respuestas_formaA() ##Objeto con herramientas de calificacion forma A
Estres=respuestas_formaEstres()
Extralaborales=respuestas_formaExtralaboral()
Sociodemografico=respuestas_socioDemografico_estudios()
Sociodemografico_cedula=respuestas_socioDemografico_cedula()
Sociodemografico_nacimiento=respuestas_socioDemografico_nacimiento()
Sociodemografico_estado=respuestas_socioDemografico_estado_civil()
Sociodemografico_vivienda=respuestas_socioDemografico_tipo_vivienda()
Sociodemografico_estrato=respuestas_socioDemografico_estrato()
Sociodemografico_sexo=respuestas_socioDemografico_sexo()
Sociodemografico_dependientes=respuestas_socioDemografico_dependientes()
Sociodemografico_duracion=respuestas_socioDemografico_duracion()
Sociodemografico_tipo_cargo=respuestas_socioDemografico_tipo_cargo()
Sociodemografico_duracion_cargo=respuestas_socioDemografico_duracion_cargo()
Sociodemografico_tipo_contrato=respuestas_socioDemografico_tipo_de_contrato()
Sociodemografico_horas_diarias=respuestas_socioDemografico_horas_diarias()
Sociodemografico_tipo_salario=respuestas_sociodemografico_tipo_salario()

def normalize(s):
    replacements = (
        ("ñ", "n"),
        ("Ñ", "n"),
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
        ("ñ", "n"),
 

    )
    for a, b in replacements:
        s = s.replace(a, b).replace(a.upper(), b.upper())
    return s
def leer_imagen(nombre):
    imagen=cv2.imread(nombre,0)
    return imagen
def mostrar_imagen(img,op):
    if op==1:
        cv2.imshow("imagen",img)
        cv2.waitKey(0)
    else:
        plt.imshow(img)
        plt.xticks([]), plt.yticks([])
        plt.show()

def calificacion(img):
    return(np.linalg.norm(img,2))

def restart_ancho(forma):
    if(forma=="SOCIO-2"):
        return(4781) ##ANCHO INIT INICIAL
    else:
        return(50)
    
def limpiarTemp():
    root=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'media/temp/'))
    for root, dirs, files in os.walk(root):
                for filename in files:
                    os.remove(root+filename)

def identificador(forma,root):
      ### VALORES PARA RECONOCIMIENTO DE TIPO DE HOJA DE RESPUESTA
    ancho_init_recon=50 # Coordenada x para rectangulo superior de reconocimiento de tipo de hoja
    ancho_end_recon=4900
    alto_init_recon=118
    alto_end_recon=260

    ### VALORES FORMA A

    ancho_init_recon_formaA=2595 # Coordenada x para rectangulo superior de reconocimiento de tipo de hoja
    ancho_end_recon_formaA=2823
    alto_init_recon_formaA=168
    alto_end_recon_formaA=260

    root=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",root))
    print(root)
    for root, dirs, files in os.walk(root):
            for filename in files:
                info=os.stat(root+filename)
                print(filename,info.st_size)
                if(info.st_size>2000000):
                    img=cv2.imread(root+filename,0)
                    recorte=img[alto_init_recon_formaA:alto_end_recon_formaA,ancho_init_recon_formaA:ancho_end_recon_formaA]

                    if(np.linalg.norm(recorte,2)<30000):
                        img = cv2.rectangle(img,(ancho_init_recon_formaA,alto_init_recon_formaA),(ancho_end_recon_formaA,alto_end_recon_formaA),(0,255,0),1)
                        if forma==("Forma A"):
                            return filename
                    else:
                        ancho_init_recon_socio=50 # Coordenada x para rectangulo superior de reconocimiento de tipo de hoja
                        ancho_end_recon_socio=190
                        alto_init_recon_socio=168
                        alto_end_recon_socio=270
                        img = cv2.rectangle(img,(ancho_init_recon_socio,alto_init_recon_socio),(ancho_end_recon_socio,alto_end_recon_socio),(0,255,0),1)
                        recorte=img[alto_init_recon_socio:alto_end_recon_socio,ancho_init_recon_socio:ancho_end_recon_socio]
                        if ((np.linalg.norm(recorte,2))<30000):
                            if forma==("SOCIO-1"):
                                    return filename

                        elif ((np.linalg.norm(recorte,2))>30000 and (np.linalg.norm(recorte,2))<40000):
                            if forma==("SOCIO-2"):
                                return filename
                else:
                    os.remove(root+filename)

        



def respuestas(img,forma):

    marcas=range(55) ## NUMERO DE MARCAS NEGRAS EN FORMA A
    suma_negras=0 ##Variable para desfase de rango para marca negra
    alto=36 ##Dimensiones de la marca alto ##NO MODIFICAR
    ancho=90##Dimensiones de la marca ancho ##NO MODIFICAR

    ancho_end=200 ##Dimensiones de area con error para marca negra
    alto_end=1050  ##Dimensiones de area con error para marca negra

    altoinit=944 ##Coordenada Y para marca con rango de error
    anchoinit=50 ##Coordenada X para marca con rango de error
    
    ancho_marcas=50 ##Coordenada X para marca con rango de error

    anchoinit_max=0## Variable para establecer marca negra dentro de rango
    altoinit_max=0 ## Variable para establecer marca negra dentro de rango

    norma_min=100000 #VALOR PARA COMPARACION MINIMA

    salto_marca_negra=100 ## Salto entre pixeles de marcas negras
    distancia_respuesta=198 ##Distancia entre puntos para respuesta

    tamano_pregunta=15##Tamano del recuadro para captura de respuesta
    distancia_marca_respesta=350 ##Distancia entre la marca negra y la primera opcion de respuesta

    altura_actual_inicial=altoinit
    altura_actual_final=alto_end

    numero_pregunta=1

    aumento_segunda_fila=1205 ##distancia entre primera fila de preguntas y segunda

    aumento_numero_pregunta=55 ## Aumento del indice de preguntas entre columnas

    estres_espacio=False ##Espacio de los formatos que no se deben tomar en cuenta
    extralaboral_espacio=False ## Espacio de los formatos que no se deben tomar en cuenta
    respuestas_totales={}

    calificaciones_parciales=[] ## LISTA PARA CALCULO DE FORMA A (EXTRALABORAL Y ESTRES)
 
    calificaciones_parciales_cedula=[] ## LISTA PARA CALCULO DE CEDULAS

    calificaciones_parciales_nacimiento=[] ## LISTA PARA CALCULO DE NACIMIENTO

    calificaciones_parciales_estado=[] ## LISTA PARA CALCULO DE ESTADO CIVIL

    calificaciones_parciales_vivienda=[] ##LISTA PARA CALCULO DE TIPO DE VIVIENDA

    calificaciones_parciales_estrato=[] ##LISTA PARA CALCULO DE TIPO DE ESTRATO

    calificaciones_parciales_sexo=[] ##LISTA PARA CALCULO DE TIPO DE SEXO

    calificaciones_parciales_dependientes=[] ##LISTA PARA CALCULO DE TIPO DE DEPENDIENTES


    calificaciones_parciales_duracion=[] ##LISTA PARA CALCULO DE DURACION EN LA EMPRESA
    
    calificaciones_parciales_tipo_cargo=[]##LISTA PARA CALCULO DE DURACION EN LA EMPRESA

    calificaciones_parciales_duracion_cargo=[] ##LISTA PARA CALCULO DE DURACION CARGO
    
    calificaciones_parciales_tipo_contrato=[]  ##LISTA PARA CALCULO DE TIPO DE CONTRATO

    calificaciones_parciales_horas_diarias=[] ##LISTA PARA CALCULO DE HORAS DIARIAS

    calificaciones_parciales_tipo_salario=[] ##LISTA PARA CALCULO DE TIPO SALARIO

    aux_calificacion_duracion_cargo_menos=False

    menos_duracion=False ##Auxiliar para definir si el empleado tiene menos de un año en la empresa

    ###SUMATORIA PARA MARCAS NEGRAS HACIA ABAJO
        

    if(forma=='SOCIO-2'): ##CAMBIOS PARA BUSQUEDA DE MARCAS
        anchoinit=4781
        ancho_marcas=4781
        ancho_end=ancho_marcas+150

    for i in marcas:
        #img = cv2.rectangle(img,(ancho_marcas,altura_actual_inicial),(ancho_end,altura_actual_final),(0,255,0),1)
        #BARRIDO EN Y
        while altura_actual_inicial+alto <= altura_actual_final:
           recorte=img[altura_actual_inicial:altura_actual_inicial+alto,anchoinit:anchoinit+ancho]
           norma=np.linalg.norm(recorte,2)
           if norma<norma_min:
               norma_min=norma
               altoinit_max=altura_actual_inicial ## Coordenada Y para maxima marca negra
           altura_actual_inicial=altura_actual_inicial+1

        
        #BARRIDO EN X
        norma_min=100000 ##VALOR PARA COMPARACION MINIMA
        while anchoinit+ancho <= ancho_end:
            recorte=img[altoinit_max:altoinit_max+alto,anchoinit:anchoinit+ancho]
            norma=np.linalg.norm(recorte,2)
            if norma<norma_min:
                norma_min=norma
                anchoinit_max=anchoinit ## Coordenada en X para maxima marca negra
            anchoinit=anchoinit+1            
        #img =cv2.rectangle(img,(anchoinit_max,altoinit_max),(anchoinit_max+ancho,altoinit_max+alto),(0,255,0),1)
        
        #centro=(anchoinit_max+45,altoinit_max+20)##Calcula el centro de la marca negra
        centro=(anchoinit_max,altoinit_max+alto)



 ##CALIFICACION FORMA A 
        if(forma=='FormaA'):
            centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
            #cv2.circle(img,centro,4,(0,0,0))
            suma=0
            ##IDENTIFICA EL ESPACIO DE RESPUESTA
            ################################################################CALIFICACION DE PREGUNTA
            calificaciones_parciales=[]
            aux_numero_pregunta=numero_pregunta
            opciones_de_respuesta=5
            for columnas in range(4):
                    if(opciones_de_respuesta>0):
                        for opcion_pregunta in range(opciones_de_respuesta):
                             ##cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                             ##cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                             aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                             calificaciones_parciales.append(aux_calificacion)
                             suma=suma+distancia_respuesta
                        opcion_elegida=calificaciones_parciales.index(min(calificaciones_parciales))
                        if(aux_numero_pregunta<126): 

                            respuesta=FormaA.CalificacionClaseFormaA(aux_numero_pregunta,opcion_elegida)
                            

                            
                        if(aux_numero_pregunta>=135 and aux_numero_pregunta<170):
                             
                             respuesta=Extralaborales.CalificacionClaseExtralaborales(aux_numero_pregunta-134,opcion_elegida)
                    
                        if(aux_numero_pregunta>=170):
                            
                            respuesta=Estres.CalificacionClaseEstres(aux_numero_pregunta-169,opcion_elegida)

                        respuestas_totales[aux_numero_pregunta]=respuesta
                        aux_numero_pregunta=numero_pregunta
                    
                    if columnas==0:
                        suma=aumento_segunda_fila
                        aux_numero_pregunta+=aumento_numero_pregunta 
                        calificaciones_parciales=[]
                    if columnas==1:
                        suma=aumento_segunda_fila*2
                        aux_numero_pregunta+=aumento_numero_pregunta*2
                        calificaciones_parciales=[]
                        if numero_pregunta>15 and numero_pregunta<25:
                            opciones_de_respuesta=0
                            aux_numero_pregunta-=aumento_numero_pregunta*2
                        else:
                            extralaboral_espacio=False
                    if columnas==2:
                        suma=aumento_segunda_fila*3+90
                        aux_numero_pregunta+=aumento_numero_pregunta*3
                        calificaciones_parciales=[]
                        opciones_de_respuesta=4
                        if (numero_pregunta>=1 and numero_pregunta<5) or (numero_pregunta>35):
                            opciones_de_respuesta=0
                        else:
                            estres_espacio=False
       
                                
        elif(forma=="SOCIO-1"):
            ##NIVEL DE ESCOLARIDAD

            distancia_marca_respesta=253
            distancia_respuesta=900
            centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
            #cv2.circle(img,centro,4,(0,0,0))
            suma=0
            aux_numero_pregunta=numero_pregunta
            opciones_de_respuesta=4
            if(numero_pregunta>=8 and numero_pregunta<=10):

                for columnas in range(1): ##NUMERO DE COLUMNAS PARA LA RESPUESTA
                        for opcion_pregunta in range(opciones_de_respuesta):
                             ##cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                             ##cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                             aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                             calificaciones_parciales.append(aux_calificacion)

                             suma=suma+distancia_respuesta
                             if(opcion_pregunta==1):
                                suma+=495  ##Distancia entre las dos colimnas de nivel de estudios
                                distancia_respuesta=1289
                opcion_respuesta=calificaciones_parciales.index(min(calificaciones_parciales))
                nivel_de_estudios=Sociodemografico.nivel_de_estudios(opcion_respuesta)
            

             
            if(numero_pregunta>=15 and numero_pregunta<=24):
                ##CEDULA DE CIUDADANIA
                distancia_marca_respesta=455 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=100 ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                ##cv2.circle(img,centro,4,(0,0,0))
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=13
                
                for columnas in range(1): ##NUMERO DE COLUMNAS PARA LA RESPUESTA
                        for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_cedula.append(aux_calificacion)
                            suma=suma+distancia_respuesta
                

                ##AÑO DE NACIMIENTO
                distancia_marca_respesta=2054 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=100 ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                ##cv2.circle(img,centro,4,(0,0,0))
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=4
                for columnas in range(1): ##NUMERO DE COLUMNAS PARA LA RESPUESTA
                        for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_nacimiento.append(aux_calificacion)
                            suma=suma+distancia_respuesta

                    #opcion_respuesta=calificaciones_parciales.index(min(calificaciones_parciales))
                    #nivel_de_estudios=Sociodemografico.nivel_de_estudios(opcion_respuesta)
                    ##print(nivel_de_estudios)   
            

            if(numero_pregunta>=12 and numero_pregunta<=18):
                ##ESTADO CIVIL
                distancia_marca_respesta=2747 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=100 ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                ##cv2.circle(img,centro,4,(0,0,0))
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=1
                for columnas in range(1): ##NUMERO DE COLUMNAS PARA LA RESPUESTA
                        for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_estado.append(aux_calificacion)
                            suma=suma+distancia_respuesta

            if(numero_pregunta>=22 and numero_pregunta <= 24) :
                ##TIPO DE VIVIENDA
                distancia_marca_respesta=2650 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=100 ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=1
                for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_vivienda.append(aux_calificacion)
                            suma=suma+distancia_respuesta
            if(numero_pregunta>=16 and numero_pregunta<=18):
                ## ESTRATO
                distancia_marca_respesta=3751 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=295 ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=3
                for columnas in range(1):
                    for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_estrato.append(aux_calificacion)
                            suma=suma+distancia_respuesta

            if(numero_pregunta>=21  and numero_pregunta<=22):
                ## SEXO
                distancia_marca_respesta=3950 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=295 ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=1
                for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_sexo.append(aux_calificacion)
                            suma=suma+distancia_respuesta 

            if(numero_pregunta>=31  and numero_pregunta<=33):
                ## PERSONAS DEPENDIENTES
                distancia_marca_respesta=255 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=300    ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=7
                for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_dependientes.append(aux_calificacion)
                            suma=suma+distancia_respuesta 

            if(numero_pregunta==41):
                ##NUMERO DE AÑOS EN LA EMPRESA
                distancia_marca_respesta=2059 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=300    ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=7

                #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                aux=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                if aux<=5000:
                    menos_duracion=True
                
                ##NUMERO DE AÑOS EN LA EMPRESA
            if(numero_pregunta==43 and not(menos_duracion)):
                distancia_marca_respesta=2059 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=98    ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=25
                for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_duracion.append(aux_calificacion)
                            suma=suma+distancia_respuesta 

                ##TIPO DE CARGO
            if(numero_pregunta >= 48 and numero_pregunta<=49 ):
                distancia_marca_respesta=465 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                distancia_respuesta=1785   ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                suma=0
                aux_numero_pregunta=numero_pregunta
                opciones_de_respuesta=2
                for opcion_pregunta in range(opciones_de_respuesta):
                            #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                            #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                            aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                            calificaciones_parciales_tipo_cargo.append(aux_calificacion)
                            suma=suma+distancia_respuesta 
       
        elif(forma=='SOCIO-2'):

                ###AÑOS DE DURACION EN EL CARGO
                centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                #cv2.circle(img,centro,4,(0,0,0))

                if(numero_pregunta==7):
                    distancia_marca_respesta=-2742 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                    centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                    suma=0
                    aux_numero_pregunta=numero_pregunta
                    opciones_de_respuesta=1
                    #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                    #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                    aux_calificacion_duracion_cargo=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                    if(aux_calificacion_duracion_cargo<5000):
                        aux_calificacion_duracion_cargo_menos=True
                    
                if(numero_pregunta==9 and not(aux_calificacion_duracion_cargo_menos)):
                            distancia_marca_respesta=-356 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                            centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                            distancia_respuesta=-100   ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                            suma=0
                            aux_numero_pregunta=numero_pregunta
                            opciones_de_respuesta=25
                            for opcion_pregunta in range(opciones_de_respuesta):
                                    #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                                    #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                                    aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                                    calificaciones_parciales_duracion_cargo.append(aux_calificacion)
                                    suma=suma+distancia_respuesta 
                
                ## TIPO DE CONTRATO
                if(numero_pregunta>=18 and numero_pregunta<=23):
                            distancia_marca_respesta=-4340 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                            centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                            distancia_respuesta=-100   ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                            suma=0
                            aux_numero_pregunta=numero_pregunta
                            opciones_de_respuesta=1
                            for opcion_pregunta in range(opciones_de_respuesta):
                                    #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                                    #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                                    aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                                    calificaciones_parciales_tipo_contrato.append(aux_calificacion)
                                    suma=suma+distancia_respuesta 
                    
                ##HORAS TRABAJADAS AL DIA
                if(numero_pregunta==28):
                    distancia_marca_respesta=-1850 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                    centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                    distancia_respuesta=-100   ## DISTANCIA ENTRE PUNTOS DE RESPUESTA
                    suma=0
                    aux_numero_pregunta=numero_pregunta
                    opciones_de_respuesta=24
                    for opcion_pregunta in range(opciones_de_respuesta):
                                    #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                                    #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                                    aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                                    calificaciones_parciales_horas_diarias.append(aux_calificacion)
                                    suma=suma+distancia_respuesta  

                if(numero_pregunta>=33 and numero_pregunta<=37):
                    if(numero_pregunta%2>0):
                        distancia_marca_respesta=-2750 ## DISTANCIA ENTRE LA MARCA NEGRA Y LA PRIMERA RESPUESTA
                        centro_=(anchoinit_max+distancia_marca_respesta,altoinit_max+alto)
                        suma=0
                        aux_numero_pregunta=numero_pregunta
                        #cv2.circle(img,(centro_[0]+suma,centro_[1]),6,(0,0,0))
                        #cv2.rectangle(img,(centro_[0]-tamano_pregunta+suma,centro_[1]-tamano_pregunta),(centro_[0]+tamano_pregunta+suma,centro_[1]+tamano_pregunta),(0,0,0))
                        aux_calificacion=calificacion(img[centro_[1]-tamano_pregunta:centro_[1]+tamano_pregunta , centro_[0]-tamano_pregunta+suma:centro_[0]+tamano_pregunta+suma])
                        calificaciones_parciales_tipo_salario.append(aux_calificacion)
                        suma=suma+distancia_respuesta 


        #########################################################################################
        suma_negras+=salto_marca_negra
        altura_actual_inicial=altoinit+suma_negras
        altura_actual_final=alto_end+suma_negras
        norma_min=100000
        anchoinit=restart_ancho(forma) ##Reestaura el ancho inicial para evitar desfases en sumatoria de barrido en X
        
        numero_pregunta+=1


    if(forma=='FormaA'):

        valores_sociodemografico={}
        respuestasFormaA={}
        respuestasExtralaboral={}
        respuestasEstres={}
        PC1=False ## PREGUNTA PC1 DE LA FORMA A QUE ANULA LAS PREGUNTAS SIGUIENTES
        PC2=False ## PREGUNTA PC2 DE LA FORMA A QUE ANULA LAS PREGUNTAS SIGUIENTES
        for i in sorted(respuestas_totales.keys()):
        ##print(i,respuestas_totales[i])
            
            if(i<126):
                
                if(not(PC1) and not(PC2)):
                    respuestasFormaA[i]=(respuestas_totales[i])
                else:
                    respuestasFormaA[i]=['NUNCA',0]
                if(i==106):
                        
                        if(respuestas_totales[i][0]=="CASI NUNCA"):
                            PC1=True
                if(i==116):
                        PC1=False
                        
                        if(respuestas_totales[i][0]=="CASI NUNCA"):
                            PC2=True
                        
                                

            if(i>=133 and i<170):
                indice=i-134
                respuestasExtralaboral[indice]=(respuestas_totales[i])
            if(i>=170):
                indice=i-169
                respuestasEstres[indice]=(respuestas_totales[i])
        print(respuestasFormaA)
        valores_sociodemografico['FormaA']=respuestasFormaA
        valores_sociodemografico['Extralaboral']=respuestasExtralaboral
        valores_sociodemografico['Estres']=respuestasEstres

    if(forma=='SOCIO-1'):
        valores_sociodemografico={}
        #print('Nivel de estudios: ',nivel_de_estudios)
        valores_sociodemografico['nivel_de_estudios']=nivel_de_estudios

        #print('Cedula: ',Sociodemografico_cedula.cedula(calificaciones_parciales_cedula))

        cedula=Sociodemografico_cedula.cedula(calificaciones_parciales_cedula)

        valores_sociodemografico['cedula']=cedula

        #print('Año de nacimiento:',Sociodemografico_nacimiento.nacimiento(calificaciones_parciales_nacimiento))

        nacimiento=Sociodemografico_nacimiento.nacimiento(calificaciones_parciales_nacimiento)
        valores_sociodemografico['ano_nacimiento']=nacimiento
        
        #print('Estado civil: ',Sociodemografico_estado.estado_civil(calificaciones_parciales_estado))
        estado_civil=Sociodemografico_estado.estado_civil(calificaciones_parciales_estado)
        valores_sociodemografico['estado_civil']=estado_civil

        #print('Tipo de vivienda: ',Sociodemografico_vivienda.vivienda(calificaciones_parciales_vivienda))
        tipo_vivienda=Sociodemografico_vivienda.vivienda(calificaciones_parciales_vivienda)
        valores_sociodemografico['tipo_vivienda']=tipo_vivienda

        #print('Estrado: ',Sociodemografico_estrato.estrato(calificaciones_parciales_estrato))
        estrato=Sociodemografico_estrato.estrato(calificaciones_parciales_estrato)
        valores_sociodemografico['estrato']=estrato

        #print('Sexo: ',Sociodemografico_sexo.sexo(calificaciones_parciales_sexo))
        sexo=Sociodemografico_sexo.sexo(calificaciones_parciales_sexo)
        valores_sociodemografico['sexo']=sexo

        #print('Cuantas personas dependen de usted: ',Sociodemografico_dependientes.dependientes(calificaciones_parciales_dependientes))
        personas_dependientes=Sociodemografico_dependientes.dependientes(calificaciones_parciales_dependientes)
        valores_sociodemografico['personas_dependientes']=personas_dependientes

        #print('Duracion en la empresa: ', Sociodemografico_duracion.duracion(calificaciones_parciales_duracion,menos_duracion))
        duracion_empresa=Sociodemografico_duracion.duracion(calificaciones_parciales_duracion,menos_duracion)
        valores_sociodemografico['duracion_empresa']=duracion_empresa        

        #print('Tipo de cargo', Sociodemografico_tipo_cargo.tipo_cargo(calificaciones_parciales_tipo_cargo))
        tipo_cargo=Sociodemografico_tipo_cargo.tipo_cargo(calificaciones_parciales_tipo_cargo)
        valores_sociodemografico['tipo_cargo']=tipo_cargo 


    if(forma=='SOCIO-2'):
        valores_sociodemografico={}
        valores_sociodemografico['duracion_cargo']=Sociodemografico_duracion_cargo.duracion(calificaciones_parciales_duracion_cargo,aux_calificacion_duracion_cargo_menos)
        valores_sociodemografico['tipo_contrato']=Sociodemografico_tipo_contrato.tipo_contrato(calificaciones_parciales_tipo_contrato)
        valores_sociodemografico['horas_diarias']=Sociodemografico_horas_diarias.horas(calificaciones_parciales_horas_diarias)
        valores_sociodemografico['tipo_salario']=Sociodemografico_tipo_salario.tipo(calificaciones_parciales_tipo_salario)

    return valores_sociodemografico
def img_op(img):
    return (cv2.resize(img,(img.shape[1]/7,img.shape[0]/7)))

def get_cedula(url,tipo_formulario):
    url=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",url))
    img=cv2.medianBlur(leer_imagen(url),3)
    rows,cols = img.shape
    M = cv2.getRotationMatrix2D((cols/2,rows/2),0.1,1)
    dst = cv2.warpAffine(img,M,(cols,rows))
    resultados=respuestas(dst,tipo_formulario)
    return(resultados)


def calificar(url,tipo_formulario,empresa,fecha,paciente):
    url=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",url))
    img=cv2.medianBlur(leer_imagen(url),3)
    rows,cols = img.shape
    M = cv2.getRotationMatrix2D((cols/2,rows/2),0.1,1)
    dst = cv2.warpAffine(img,M,(cols,rows))
    resultados=respuestas(dst,tipo_formulario)

    try:
        try:
            os.mkdir(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../","static/"+empresa)))
        except:
            pass
        
    except Exception as e:
        print(e)
    try:
        try:
            os.mkdir(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../","static/"+empresa+'/'+paciente+'-'+str(resultados['cedula']))))
        except:
            pass
        
    except Exception  as e:
        print("El fichero ya esta creado para la cedula"+ str(e))
        
    cv2.imwrite(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'static/'+empresa+'/'+paciente+'-'+str(resultados['cedula'])+'/'+str(resultados['cedula'])+'-'+tipo_formulario+'.png')),img_op(cv2.imread(url)))
    
    return(resultados)

def calificar_socio_2(url,tipo_formulario,cedula,empresa,fecha,paciente):
    url=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",url))
    img=cv2.medianBlur(leer_imagen(url),3)
    rows,cols = img.shape
    M = cv2.getRotationMatrix2D((cols/2,rows/2),0.2,1)
    dst = cv2.warpAffine(img,M,(cols,rows))
    resultados=respuestas(dst,tipo_formulario)
    cv2.imwrite(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'static/'+empresa+'/'+paciente+'-'+cedula+'/'+cedula+'-'+tipo_formulario+'.png')),img_op(cv2.imread(url)))
    
    return(resultados)

    
def calificar_forma_a(url,tipo_formulario,cedula,empresa,fecha,paciente):
    url=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",url))
    img=cv2.medianBlur(leer_imagen(url),3)
    rows,cols = img.shape
    M = cv2.getRotationMatrix2D((cols/2,rows/2),0.3,1)
    dst = cv2.warpAffine(img,M,(cols,rows))
    resultados=respuestas(dst,'FormaA')
        #print("El fichero ya esta creado para la cedula"+ resultados["cedula"])
    cv2.imwrite(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'static/'+empresa+'/'+paciente+'-'+cedula+'/'+cedula+'-'+tipo_formulario+'.png')),img_op(cv2.imread(url)))
    
    return(resultados)

    
    

    



    
