# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import cv2
from django.shortcuts import render
from django.core.files.storage import FileSystemStorage 
import os
from django.http import JsonResponse,HttpResponse
from . import calificador
from models import T11Pacientes,T22Ordenes
import json
from django.core import serializers
import os
from excel_response import ExcelResponse
import xlsxwriter
import zipfile
from django.db import connection
import shutil
# Create your views here.

def inicio(req):
	if(req.method=='POST'):
		
		rootImagenSocio1=calificador.identificador('SOCIO-1',"media/temp/")
		
		# fs=FileSystemStorage(location='media/temp/')
		
		# fs.save('temporal1.png',file

		

		
		datos_iniciales=calificador.get_cedula('media/temp/'+rootImagenSocio1,'SOCIO-1')
		
		cedula_zip=datos_iniciales['cedula']
		cursor=connection.cursor()
		cursor.execute("select t11id,t11primernombre,coalesce(t11segundonombre,' '),t11primerapellido,coalesce(t11segundoapellido,' '),t11nit,t11pacientes.created_at,t02razonsocial,prof.t01nombre,t22id,t22cargo,depT.t01nombre,ciuT.t01nombre,depR.t01nombre,ciuR.t01nombre from t11pacientes inner join t22ordenes on t11id=t22paciente and t22estado=0 inner join t23examenesorden on t23orden=t22id and t23examen=81 inner join t02clientes on t22cliente=t02id left join t01directorio prof on t22profesion = prof.t01id left join t01directorio depR on t22departamentoresidencia = depR.t01id left join t01directorio depT on t22departamento = depT.t01id left join t01directorio ciuR on t22ciudadresidencia = ciuR.t01id left join t01directorio ciuT on t22ciudad = ciuT.t01id where t11nit='"+cedula_zip+"'")
		row = cursor.fetchone()
		
		if(cursor.rowcount==0):
			calificador.limpiarTemp()
			return(render(req,'SinEscaner.html',{'mensaje':'EL PACIENTE CON LA CEDULA '+cedula_zip+' NO CUENTA CON UNA ORDEN ACTIVA PARA BATERIA DE RIESGO'}))
		req.session['datos_trabajador_nombre']=row[1]+' '+row[2]+' '+row[3]+' '+row[4]
		req.session['datos_trabajador_cedula']=row[5]
		req.session['datos_trabajador_fecha_aplicacion']=row[6].strftime("%m-%d-%Y")
		req.session['datos_trabajador_nombre_empresa']=row[7]
		req.session['datos_trabajador_profesion']=row[8]
		req.session['orden']=row[9]
		req.session['cargo']=row[10]
		req.session['dept']=row[11]
		req.session['ciut']=row[12]
		req.session['depr']=row[13]
		req.session['ciur']=row[14]
		req.session['datos_trabajador_nombre']=req.session['datos_trabajador_nombre'].replace('Ñ','N')
		req.session['datos_trabajador_nombre']=req.session['datos_trabajador_nombre'].replace('Ó','O')
		req.session['datos_trabajador_nombre']=req.session['datos_trabajador_nombre'].replace('Á','A')
		req.session['datos_trabajador_nombre']=req.session['datos_trabajador_nombre'].replace('É','E')
		req.session['datos_trabajador_nombre']=req.session['datos_trabajador_nombre'].replace('Í','I')
		req.session['datos_trabajador_nombre']=req.session['datos_trabajador_nombre'].replace('Ú','U')
		
		respuestas=calificador.calificar('media/temp/'+rootImagenSocio1,'SOCIO-1',req.session['datos_trabajador_nombre_empresa'],req.session['datos_trabajador_fecha_aplicacion'],req.session['datos_trabajador_nombre'])
		try:
			pass
		except:
			return(render(req,'SinEscaner.html',{'mensaje':'NO SE ENCUENTRA NINGUN ARCHIVO ESCANEADO, POR FAVOR ASEGURESE DE ESCANEAR LAS TRES PARTES DE LA BATERIA'}))
		req.session['sociodemografico_1']=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'media/temp/'+rootImagenSocio1))
		respuestas['url']=req.session['datos_trabajador_nombre_empresa']+'/'+req.session['datos_trabajador_nombre']+'-'+str(respuestas['cedula'])+'/'+str(respuestas['cedula'])+'-SOCIO-1.png'
		
		#os.remove("media/temp/"+rootImagenFormaA)
		
		req.session['datos_trabajador_cedula']=str(respuestas['cedula'])
		req.session['datos_trabajador_nivel_estudio']=str(respuestas['nivel_de_estudios'])
		req.session['datos_trabajador_ano_nacimiento']=str(respuestas['ano_nacimiento'])
		req.session['datos_trabajador_estado_civil']=str(respuestas['estado_civil'])
		req.session['datos_trabajador_estrato']=str(respuestas['estrato'])
		req.session['datos_trabajador_tipo_vivienda']=str(respuestas['tipo_vivienda'])
		req.session['datos_trabajador_sexo']=str(respuestas['sexo'])
		req.session['datos_trabajador_personas_dependientes']=str(respuestas['personas_dependientes'])
		req.session['datos_trabajador_duracion_empresa']=str(respuestas['duracion_empresa'])
		req.session['datos_trabajador_tipo_cargo']=str(respuestas['tipo_cargo'])
		req.session['url_grafica']='http://199.250.220.160/~psicos6/erp/public/validacion_baterias/'+str(req.session['orden'])+'/81'
		#os.remove(req.session['sociodemografico_1'])
		return(render(req,'sociodemografico_1.html',{"form":respuestas}))
	else:
		return(render(req,'index.html'))	

# def sociodemografico_1(req):
# 	if(req.method=="POST"):	
# 		if('cambio_img' in req.POST):
# 			pass
# 		else:
# 			nombre_paciente=req.POST.get('nombre_paciente')
# 			departamento_trabajo=req.POST.get('departamento_trabajo')
# 			municipio_trabajo=req.POST.get('municipio_trabajo')
# 			departamento_trabajo=req.POST.get('departamento_trabajo')
# 			municipio_vivienda=req.POST.get('municipio_vivienda')
# 			cargo=req.POST.get('cargo')
# 			area=req.POST.get('area')
# 			req.session['datos_trabajador_nombre']=req.POST.get('nombre_paciente').upper()
# 			req.session['datos_trabajador_municipio_trabajo']=req.POST.get('municipio_trabajo')
# 			req.session['datos_trabajador_municipio_vivienda']=req.POST.get('municipio_vivienda')
# 			req.session['datos_trabajador_departamento_trabajo']=Departamentos.objects.get(id_departamento=req.POST.get('departamento_trabajo')).departamento
# 			req.session['datos_trabajador_departamento_vivienda']=Departamentos.objects.get(id_departamento=req.POST.get('departamento_vivienda')).departamento
# 			req.session['datos_trabajador_cargo']=req.POST.get('cargo')
# 			req.session['datos_trabajador_area']=req.POST.get('area')
# 			req.session['datos_trabajador_profesion']=req.POST.get('profesion')
# 			req.session['datos_trabajador_nombre_empresa']=req.POST.get('empresa').upper()
# 			req.session['datos_trabajador_fecha_aplicacion']=req.POST.get('aplicacion_fecha')


# 		rootImagenSocio1=calificador.identificador('SOCIO-1',"media/temp/")
		
# 		# fs=FileSystemStorage(location='media/temp/')
		
# 		# fs.save('temporal1.png',file)
		
# 		try:
# 			respuestas=calificador.calificar('media/temp/'+rootImagenSocio1,'SOCIO-1',req.session['datos_trabajador_nombre_empresa'],req.session['datos_trabajador_fecha_aplicacion'],req.session['datos_trabajador_nombre'])

# 		except:
# 			return(render(req,'SinEscaner.html',{'mensaje':'NO SE ENCUENTRA NINGUN ARCHIVO ESCANEADO, POR FAVOR ASEGURESE DE ESCANEAR LAS TRES PARTES DE LA BATERIA'}))
# 		req.session['sociodemografico_1']='media/temp/'+rootImagenSocio1

# 		respuestas['url']=req.session['datos_trabajador_nombre_empresa']+'/'+req.session['datos_trabajador_nombre']+'-'+str(respuestas['cedula'])+'/'+str(respuestas['cedula'])+'-SOCIO-1.png'
		
# 		os.remove("media/temp/"+rootImagenFormaA)

# 		req.session['datos_trabajador_cedula']=str(respuestas['cedula'])
# 		req.session['datos_trabajador_nivel_estudio']=str(respuestas['nivel_de_estudios'])
# 		req.session['datos_trabajador_ano_nacimiento']=str(respuestas['ano_nacimiento'])
# 		req.session['datos_trabajador_estado_civil']=str(respuestas['estado_civil'])
# 		req.session['datos_trabajador_estrato']=str(respuestas['estrato'])
# 		req.session['datos_trabajador_tipo_vivienda']=str(respuestas['tipo_vivienda'])
# 		req.session['datos_trabajador_sexo']=str(respuestas['sexo'])
# 		req.session['datos_trabajador_personas_dependientes']=str(respuestas['personas_dependientes'])
# 		req.session['datos_trabajador_duracion_empresa']=str(respuestas['duracion_empresa'])
# 		req.session['datos_trabajador_tipo_cargo']=str(respuestas['tipo_cargo'])

# 		#os.remove(req.session['sociodemografico_1'])
# 		return(render(req,'sociodemografico_1.html',{"form":respuestas}))
# 		# if(file.size>10000):
# 		# 	context
# 	else:
# 			return(render(req,'index.html',{'municipios':Municipios.objects.all(),'departamentos':Departamentos.objects.all(),'areas':AreaEmpresas.objects.order_by('nombre'),'cargos':Cargos.objects.order_by('nombre'),'profesiones':Profesiones.objects.order_by('nombre')}))	

def sociodemografico_2(req):
	if(req.method=="POST"):
		
		if('cambio_img' in req.POST):
				pass

		rootImagenSocio2=calificador.identificador('SOCIO-2',"media/temp/")

		try:
			respuestas=calificador.calificar_socio_2('media/temp/'+rootImagenSocio2,'SOCIO-2',req.session['datos_trabajador_cedula'],req.session['datos_trabajador_nombre_empresa'],req.session['datos_trabajador_fecha_aplicacion'],req.session['datos_trabajador_nombre'])
		except:
			return(render(req,'SinEscaner.html',{'mensaje':'NO SE ENCUENTRA NINGUN ARCHIVO ESCANEADO, POR FAVOR ASEGURESE DE ESCANEAR LAS TRES PARTES DE LA BATERIA'}))
		req.session['sociodemografico_2']=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'media/temp/'+rootImagenSocio2))

		respuestas['url']=req.session['datos_trabajador_nombre_empresa']+'/'+req.session['datos_trabajador_nombre']+'-'+req.session['datos_trabajador_cedula']+'/'+req.session['datos_trabajador_cedula']+'-SOCIO-2.png'

		req.session['datos_trabajador_duracion_cargo']=str(respuestas["duracion_cargo"])
		req.session['datos_trabajador_tipo_contrato']=respuestas['tipo_contrato']
		req.session['datos_trabajador_horas_diarias']=respuestas['horas_diarias']
		req.session['datos_trabajador_tipo_salario']=respuestas['tipo_salario']

		if(req.session['datos_trabajador_tipo_cargo']== "JEFATURA-TIENE PERSONAL A CARGO" or req.session['datos_trabajador_tipo_cargo']=="PROFESIONAL,ANALISTA,TECNICO,TECNOLOGO"):
			req.session['url_tipo_forma']='FormaA'
		else:
			req.session['url_tipo_forma']='FormaB' ###PONER FORMA B EN PRODUCCION

		os.remove(req.session['sociodemografico_2'])
		return(render(req,'sociodemografico_2.html',{"form":respuestas}))

	else:
		if('datos_trabajador_cedula' in req.session):
			respuestas={}
			respuestas['url']=req.session['datos_trabajador_nombre_empresa']+'/'+req.session['datos_trabajador_nombre']+'-'+req.session['datos_trabajador_cedula']+'/'+req.session['datos_trabajador_cedula']+'-SOCIO-2.png'
			return(render(req,'sociodemografico_2.html',{"form":respuestas}))
		else:
			return(render(req,'sociodemografico_2.html'))


def formaA(req):
	if(req.method=="POST"):
		if (not ('CheckFormA' in req.POST)) or ('cambio_img' in req.POST):
			
			
			
			rootImagenFormaA=calificador.identificador('Forma A',"media/temp/")
			
			# file=req.FILES['formaA']
			
			# os.remove("media/temp/temporal1.png")
			# fs=FileSystemStorage(location='media/temp/')
			# fs.save('temporal1.png',file)
			
			try:
				respuestas=calificador.calificar_forma_a('media/temp/'+rootImagenFormaA,req.session['url_tipo_forma'],req.session['datos_trabajador_cedula'],req.session['datos_trabajador_nombre_empresa'],req.session['datos_trabajador_fecha_aplicacion'],req.session['datos_trabajador_nombre'])
			except:
				return(render(req,'SinEscaner.html',{'mensaje':'NO SE ENCUENTRA NINGUN ARCHIVO ESCANEADO, POR FAVOR ASEGURESE DE ESCANEAR LAS TRES PARTES DE LA BATERIA'}))
			req.session['forma_a']=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'media/temp/'+rootImagenFormaA))
			respuestas['url']=""
			respuestas['url']=req.session['datos_trabajador_nombre_empresa']+'/'+req.session['datos_trabajador_nombre']+'-'+req.session['datos_trabajador_cedula']+'/'+req.session['datos_trabajador_cedula']+'-'+req.session['url_tipo_forma']+'.png'
			
			formaA=[]
			Extralaboral=[]
			Estres=[]
			req.session['RespuestasformaA']={}
			req.session['RespuestasExtralaboral']={}
			req.session['RespuestasEstres']={}
			req.session['RespuestasformaA']=respuestas['FormaA'] ##GUARDAMOS EN LA SESSION LAS RESPUESTAS DE FORMA A PARA PEDIR CONFIRMACION EN VISTA
			req.session['RespuestasExtralaboral']= respuestas['Extralaboral']
			req.session['RespuestasEstres']= respuestas['Estres']

			for i in respuestas['FormaA']:
				formaA.append([respuestas['FormaA'][i][0],respuestas['FormaA'][i][1],i])

			for i in respuestas['Extralaboral']:
				Extralaboral.append([respuestas['Extralaboral'][i][0],respuestas['Extralaboral'][i][1],i])
				
			for i in respuestas['Estres']:
				Estres.append([respuestas['Estres'][i][0],respuestas['Estres'][i][1],i])

		
			os.remove(req.session['forma_a'])
			return(render(req,'formaA.html',{"form":respuestas,"formaA":formaA,'Extralaboral':Extralaboral,'Estres':Estres}))

		else:
			numero_pregunta=[]
			respuesta_pregunta=[]
			valor_numerico=[]						
			workbook =  xlsxwriter.Workbook(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'static/'+req.session['datos_trabajador_nombre_empresa']+'/'+req.session['datos_trabajador_nombre']+'-'+str(req.session['datos_trabajador_cedula'])+'/Resultado'+'-'+ req.session['datos_trabajador_cedula']+'.xlsx')))
			worksheet = workbook.add_worksheet(req.session['datos_trabajador_cedula']+req.session['url_tipo_forma'])
			worksheet.write('A1', 'clave') 
			worksheet.write('A2', 'Fecha de aplicacion') 
			worksheet.write('A3', 'Nombre del paciente') 
			worksheet.write('A4', 'Cedula') 
			worksheet.write('A5', 'Empresa') 			
			worksheet.write('A6', 'Ultimo nivel de estudios') 
			worksheet.write('A7', 'Año de nacimiento') 
			worksheet.write('A8', 'Estado Civil') 
			worksheet.write('A9', 'Tipo de vivienda') 
			worksheet.write('A10', 'Estrato') 
			worksheet.write('A11', 'Sexo') 
			worksheet.write('A12', 'Profesion u ocupacion') 
			worksheet.write('A13', 'Numero de personas dependientes') 
			worksheet.write('A14', 'Lugar donde trabaja') 
			worksheet.write('A15', 'Lugar donde reside') 
			worksheet.write('A16', 'Hace cuantos años trabaja en la empresa')
			worksheet.write('A17', 'Tipo de cargo')
			worksheet.write('A18', 'Nombre del cargo que ocupa en la empresa')
			worksheet.write('A19', 'Hace cuantos años desempeña el cargo')
			worksheet.write('A20', 'Nombre del departamento o area en que trabaja')
			worksheet.write('A21', 'Tipo de contrato')
			worksheet.write('A22', 'Horas diarias trabajadas')
			worksheet.write('A23', 'Tipo de salario')
			worksheet.write('A24', 'Tipo de Formulario')

			worksheet.write('B1', 'valor') 
			worksheet.write('C1', 'numero') 
			worksheet.write('B2', req.session['datos_trabajador_fecha_aplicacion']) 
			worksheet.write('B3', req.session['datos_trabajador_nombre']) 
			worksheet.write('B4', req.session['datos_trabajador_cedula']) 
			worksheet.write('B5', req.session['datos_trabajador_nombre_empresa']) 			
			worksheet.write('B6', req.session['datos_trabajador_nivel_estudio']) 
			worksheet.write('B7', req.session['datos_trabajador_ano_nacimiento']) 
			worksheet.write('B8', req.session['datos_trabajador_estado_civil'])
			worksheet.write('B9', req.session['datos_trabajador_tipo_vivienda'])
			worksheet.write('B10', req.session['datos_trabajador_estrato'])
			worksheet.write('B11', req.session['datos_trabajador_sexo'])
			worksheet.write('B12', req.session['datos_trabajador_profesion']) 
			worksheet.write('B13', req.session['datos_trabajador_personas_dependientes']) 
			#worksheet.write('B14', req.session['datos_trabajador_departamento_trabajo']+'-'+req.session['datos_trabajador_municipio_trabajo']) 
			#worksheet.write('B15', req.session['datos_trabajador_departamento_vivienda']+'-'+req.session['datos_trabajador_municipio_vivienda']) 
			worksheet.write('B16', req.session['datos_trabajador_duracion_empresa'])
			worksheet.write('B17', req.session['datos_trabajador_tipo_cargo'])
			#worksheet.write('B18', req.session['datos_trabajador_cargo'])
			worksheet.write('B19', req.session['datos_trabajador_duracion_cargo'])
			#worksheet.write('B20', req.session['datos_trabajador_area'])
			worksheet.write('B21', req.session['datos_trabajador_tipo_contrato'])
			worksheet.write('B22', req.session['datos_trabajador_horas_diarias'])
			worksheet.write('B23', req.session['datos_trabajador_tipo_salario'])
			worksheet.write('B24', req.session['url_tipo_forma'])

			worksheet.write('A25', 'Pregunta') 
			worksheet.write('B25', 'Respuesta') 
			worksheet.write('C26', 'Valor Numerico') 
			for i in sorted(map(lambda x: int (x),req.session['RespuestasformaA'].keys())):
						
						worksheet.write(i+24,0,i)
						worksheet.write(i+24,1,req.session['RespuestasformaA'][str(i)][0])
						worksheet.write(i+24,2,req.session['RespuestasformaA'][str(i)][1])

			### LOGICA PARA GENERACION DE ARCHIVO EXCEL
			ExtraIndex=i+25

			worksheet.write(ExtraIndex,0,'Extralaboral')

			for j in sorted(map(lambda x: int (x),req.session['RespuestasExtralaboral'].keys())):
						worksheet.write(j+ExtraIndex,0,j)
						worksheet.write(j+ExtraIndex,1,req.session['RespuestasExtralaboral'][str(j)][0])
						worksheet.write(j+ExtraIndex,2,req.session['RespuestasExtralaboral'][str(j)][1])
			
			EstresIndex=j+ExtraIndex+1
			worksheet.write(EstresIndex,0,'Estres') 
			for k in sorted(map(lambda x: int (x),req.session['RespuestasEstres'].keys())):
						worksheet.write(k+EstresIndex,0,k)
						worksheet.write(k+EstresIndex,1,req.session['RespuestasEstres'][str(k)][0])
						worksheet.write(k+EstresIndex,2,req.session['RespuestasEstres'][str(k)][1])
			
			workbook.close()
			
			
			# se crea el archivo zip

			root_zip=os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",'static/'+req.session['datos_trabajador_nombre_empresa']+'/'+req.session['datos_trabajador_nombre']+'-'+str(req.session['datos_trabajador_cedula'])+'/'))
			zipf = zipfile.ZipFile('//server0/Baterias/'+str(req.session['orden'])+'.zip', 'w', zipfile.ZIP_DEFLATED)
			zipf.write(root_zip+'/'+req.session['datos_trabajador_cedula']+'-'+req.session['url_tipo_forma']+'.png',req.session['datos_trabajador_cedula']+'-'+req.session['url_tipo_forma']+'.png')
			zipf.write(root_zip+'/'+req.session['datos_trabajador_cedula']+'-'+'SOCIO-1.png',req.session['datos_trabajador_cedula']+'-'+'SOCIO-1.png')
			zipf.write(root_zip+'/'+req.session['datos_trabajador_cedula']+'-'+'SOCIO-2.png',req.session['datos_trabajador_cedula']+'-'+'SOCIO-2.png')
			zipf.write(root_zip+'/'+'Resultado-'+req.session['datos_trabajador_cedula']+'.xlsx','Resultado-'+req.session['datos_trabajador_cedula']+'.xlsx')
			zipf.close()

			
			
			try:
				os.remove(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",req.session['sociodemografico_1'])))
				os.remove(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",req.session['sociodemografico_2'])))
				os.remove(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),os.path.join("../",req.session['forma_a'])))
			except Exception as e:
				pass
#

			return (render(req,'BateriaFinalizada.html'))
			#return(render(req,'index.html',{'municipios':Municipios.objects.all(),'departamentos':Departamentos.objects.all(),'areas':AreaEmpresas.objects.order_by('nombre'),'cargos':Cargos.objects.order_by('nombre'),'profesiones':Profesiones.objects.order_by('nombre'),"successSave":'La bateria de '+req.session['datos_trabajador_nombre']+' Fue generada exitosamente'}))
	return(render(req,'formaA.html'))



def get_municipios(req):
		departamento=req.GET.get('departamento_id')
		
		municipios=Municipios.objects.filter(departamento_id=departamento)
		
		json_data = serializers.serialize('json', municipios)
		return HttpResponse(json_data, content_type='application/json')

def add_area(req):
	if req.method=="POST":
		nombre_=req.POST.get("nombre")
		if not(AreaEmpresas.objects.filter(nombre=nombre_).exists()):
			AreaEmpresas.objects.create(nombre=(nombre_.upper()))
			return(render(req,'add_area.html',{'mensaje':nombre_.upper()+': Creado correctamente'}))
		else:
			return(render(req,'add_area.html',{'mensaje':nombre_.upper()+': Ya se encuentra en nuestra base de datos'}))
	else:
		return(render(req,'add_area.html'))

def add_cargo(req):
	if 'nombre' in req.GET:
		nombre_=req.GET.get("nombre")
		if not(Cargos.objects.filter(nombre=nombre_).exists()):
			Cargos.objects.create(nombre=(nombre_.upper()))
			return(render(req,'add_cargo.html',{'mensaje':nombre_.upper()+': Creado correctamente'}))
		else:
			return(render(req,'add_cargo.html',{'mensaje':nombre_.upper()+': Ya se encuentra en nuestra base de datos'}))
	else:
		return(render(req,'add_cargo.html'))

def add_profesion(req):
	if 'nombre' in req.GET:
		nombre_=req.GET.get("nombre")
		if not(Profesiones.objects.filter(nombre=nombre_).exists()):
			Profesiones.objects.create(nombre=(nombre_.upper()))
			return(render(req,'add_profesion.html',{'mensaje':nombre_.upper()+': Creado correctamente'}))
		else:
			return(render(req,'add_profesion.html',{'mensaje':nombre_.upper()+': Ya se encuentra en nuestra base de datos'}))
	else:
		return(render(req,'add_profesion.html'))

