# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Jobs(models.Model):
    id = models.BigAutoField(primary_key=True)
    queue = models.CharField(max_length=255)
    payload = models.TextField()
    attempts = models.SmallIntegerField()
    reserved_at = models.IntegerField(blank=True, null=True)
    available_at = models.IntegerField()
    created_at = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'jobs'


class Migrations(models.Model):
    migration = models.CharField(max_length=255)
    batch = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'migrations'


class PasswordResets(models.Model):
    email = models.TextField(blank=True, null=True)
    token = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'password_resets'


class Sys01Usuarios(models.Model):
    sys01id = models.AutoField(primary_key=True)
    sys01username = models.CharField(max_length=50, blank=True, null=True)
    password = models.TextField(blank=True, null=True)
    sys01email = models.TextField(blank=True, null=True)
    remember_token = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    sys01sedes = models.TextField(blank=True, null=True)  # This field type is a guess.
    sys01sede = models.IntegerField(blank=True, null=True)
    sys01empleado = models.IntegerField(blank=True, null=True)
    sys01tipo = models.IntegerField(blank=True, null=True)
    sys01consultorio = models.IntegerField(blank=True, null=True)
    sys01activo = models.NullBooleanField()
    sys01apellidos = models.CharField(max_length=250, blank=True, null=True)
    sys01nombres = models.CharField(max_length=250, blank=True, null=True)
    sys01sedetrabajo = models.IntegerField(blank=True, null=True)
    sys01ordenactual = models.IntegerField(blank=True, null=True)
    sys01cod = models.IntegerField(blank=True, null=True)
    sys01cliente = models.IntegerField(blank=True, null=True)
    sys01agendar = models.NullBooleanField()
    sys01profesiograma = models.NullBooleanField()
    sys01crearusuarios = models.NullBooleanField()
    sys01telefono = models.CharField(max_length=50, blank=True, null=True)
    sys01cod2 = models.IntegerField(blank=True, null=True)
    sys01cod3 = models.IntegerField(blank=True, null=True)
    sys01actual = models.NullBooleanField()
    sys01coordinador = models.NullBooleanField()
    sys01ips = models.NullBooleanField()
    sys01certificados = models.NullBooleanField()
    sys01actualizado = models.NullBooleanField()
    sys01laboratorios = models.NullBooleanField()
    sys01licenciaocupacional = models.NullBooleanField()
    sys01turnoactual = models.IntegerField(blank=True, null=True)
    sys01armas = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'sys01usuarios'


class Sys02Menus(models.Model):
    sys02id = models.IntegerField(primary_key=True)
    sys02nombre = models.CharField(max_length=50, blank=True, null=True)
    sys02orden = models.IntegerField(blank=True, null=True)
    sys02ruta = models.TextField(blank=True, null=True)
    sys02imagen = models.CharField(max_length=100, blank=True, null=True)
    sys02padre = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sys02menus'


class Sys03Permisos(models.Model):
    sys03usuario = models.IntegerField()
    sys03menu = models.IntegerField()
    sys03acceso = models.NullBooleanField()
    sys03crear = models.NullBooleanField()
    sys03editar = models.NullBooleanField()
    sys03eliminar = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    sys03id = models.AutoField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'sys03permisos'


class Sys04Auditorias(models.Model):
    sys04id = models.AutoField(primary_key=True)
    sys04fecha = models.DateTimeField(blank=True, null=True)
    sys04ip = models.CharField(max_length=100, blank=True, null=True)
    sys04equipo = models.CharField(max_length=100, blank=True, null=True)
    sys04tabla = models.CharField(max_length=100, blank=True, null=True)
    sys04accion = models.CharField(max_length=50, blank=True, null=True)
    sys04usuario = models.IntegerField(blank=True, null=True)
    sys04datos = models.TextField(blank=True, null=True)
    sys04pk = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sys04auditorias'


class Sys05Parametros(models.Model):
    sys05id = models.AutoField(primary_key=True)
    sys05sede = models.IntegerField(blank=True, null=True)
    sys05nombre = models.TextField(blank=True, null=True)
    sys05valor = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    sys05tipo = models.CharField(max_length=50, blank=True, null=True)
    sys05grupo = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sys05parametros'


class Sys06Alertas(models.Model):
    sys06id = models.AutoField(primary_key=True)
    sys06usuario = models.IntegerField(blank=True, null=True)
    sys06tipo = models.CharField(max_length=100, blank=True, null=True)
    sys06nombre = models.TextField(blank=True, null=True)
    sys06descripcion = models.TextField(blank=True, null=True)
    sys06descripcion2 = models.TextField(blank=True, null=True)
    sys06fecha = models.DateField(blank=True, null=True)
    sys06url = models.TextField(blank=True, null=True)
    sys06pk = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    sys06sede = models.IntegerField(blank=True, null=True)
    sys06cumplida = models.NullBooleanField()
    sys06revisada = models.NullBooleanField()
    sys06rechazada = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'sys06alertas'


class Sys07Parametrosgenerales(models.Model):
    sys07id = models.AutoField(primary_key=True)
    sys07nombre = models.CharField(max_length=100, blank=True, null=True)
    sys07valor = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sys07parametrosgenerales'


class Sys08Permisosespeciales(models.Model):
    sys08id = models.AutoField(primary_key=True)
    sys08nombre = models.CharField(max_length=100, blank=True, null=True)
    sys08usuario = models.IntegerField(blank=True, null=True)
    sys08permiso = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sys08permisosespeciales'


class Sys09Tickets(models.Model):
    sys09id = models.AutoField(primary_key=True)
    sys09fecha = models.DateTimeField(blank=True, null=True)
    sys09usuario = models.IntegerField(blank=True, null=True)
    sys09descripcion = models.TextField(blank=True, null=True)
    sys09solucionado = models.NullBooleanField()
    sys09fechasolucionado = models.DateTimeField(blank=True, null=True)
    sys09descripcionsolucion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    sys09email = models.TextField(blank=True, null=True)
    sys09leido = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'sys09tickets'


class Sys10Sesiones(models.Model):
    sys10id = models.AutoField(primary_key=True)
    sys10usuario = models.IntegerField(blank=True, null=True)
    sys10fecha = models.DateTimeField(blank=True, null=True)
    sys10ip = models.TextField(blank=True, null=True)
    sys10equipo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sys10sesiones'


class T01Directorio(models.Model):
    t01id = models.AutoField(primary_key=True)
    t01nombre = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t01grupo = models.TextField(blank=True, null=True)
    t01valor = models.TextField(blank=True, null=True)
    t01padre = models.IntegerField(blank=True, null=True)
    t01campo = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't01directorio'


class T02Clientes(models.Model):
    t02id = models.AutoField(primary_key=True)
    t02nit = models.CharField(max_length=100, blank=True, null=True)
    t02razonsocial = models.TextField(blank=True, null=True)
    t02contacto = models.CharField(max_length=250, blank=True, null=True)
    t02telefono = models.CharField(max_length=100, blank=True, null=True)
    t02direccion = models.CharField(max_length=250, blank=True, null=True)
    t02departamento = models.IntegerField(blank=True, null=True)
    t02ciudad = models.IntegerField(blank=True, null=True)
    t02sector = models.IntegerField(blank=True, null=True)
    t02email = models.CharField(max_length=250, blank=True, null=True)
    t02nombrecomercial = models.TextField(blank=True, null=True)
    t02fax = models.CharField(max_length=150, blank=True, null=True)
    t02celular = models.CharField(max_length=200, blank=True, null=True)
    t02fechaconstitucion = models.DateField(blank=True, null=True)
    t02actividad = models.IntegerField(blank=True, null=True)
    t02regimen = models.IntegerField(blank=True, null=True)
    t02cupo = models.FloatField(blank=True, null=True)
    t02categoria = models.IntegerField(blank=True, null=True)
    t02plazo = models.IntegerField(blank=True, null=True)
    t02formapago = models.IntegerField(blank=True, null=True)
    t02descuento = models.FloatField(blank=True, null=True)
    t02dv = models.IntegerField(blank=True, null=True)
    t02accesoips = models.NullBooleanField()
    t02enviocorreo = models.NullBooleanField()
    t02certificado = models.NullBooleanField()
    t02conceptos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t02contactotesoreria = models.CharField(max_length=250, blank=True, null=True)
    t02telefonotesoreria = models.CharField(max_length=100, blank=True, null=True)
    t02contactorecursos = models.CharField(max_length=250, blank=True, null=True)
    t02telefonorecursos = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t02tipo = models.CharField(max_length=10, blank=True, null=True)
    t02emailtesoreria = models.CharField(max_length=200, blank=True, null=True)
    t02emailrecursos = models.CharField(max_length=200, blank=True, null=True)
    t02primerapellido = models.CharField(max_length=100, blank=True, null=True)
    t02segundonombre = models.CharField(max_length=100, blank=True, null=True)
    t02primernombre = models.CharField(max_length=100, blank=True, null=True)
    t02segundoapellido = models.CharField(max_length=100, blank=True, null=True)
    t02padre = models.IntegerField(blank=True, null=True)
    t02ocultos = models.NullBooleanField()
    t02padres = models.TextField(blank=True, null=True)  # This field type is a guess.
    t02armas = models.NullBooleanField()
    t02prioridad = models.IntegerField(blank=True, null=True)
    t02efectivo = models.NullBooleanField()
    t02misionempresa = models.NullBooleanField()
    t02particular = models.NullBooleanField()
    t02alturas = models.NullBooleanField()
    t02relacion = models.NullBooleanField()
    t02fecharelacion = models.IntegerField(blank=True, null=True)
    t02cortefacturacion = models.IntegerField(blank=True, null=True)
    t02fechafacturacion = models.IntegerField(blank=True, null=True)
    t02observacionesfacturacion = models.TextField(blank=True, null=True)
    t02bloqueoacceso = models.NullBooleanField()
    t02bloqueoagendamiento = models.NullBooleanField()
    t02soportefactura = models.NullBooleanField()
    t02usuario = models.IntegerField(blank=True, null=True)
    t02observacionfacturacion = models.TextField(blank=True, null=True)
    t02observacionagendamiento = models.TextField(blank=True, null=True)
    t02autorizadofacturar = models.NullBooleanField()
    t02calificacion = models.FloatField(blank=True, null=True)
    t02contrato = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't02clientes'


class T03Proveedores(models.Model):
    t03id = models.AutoField(primary_key=True)
    t03nit = models.CharField(max_length=100, blank=True, null=True)
    t03razonsocial = models.TextField(blank=True, null=True)
    t03contacto = models.CharField(max_length=250, blank=True, null=True)
    t03telefono = models.CharField(max_length=100, blank=True, null=True)
    t03direccion = models.CharField(max_length=250, blank=True, null=True)
    t03departamento = models.IntegerField(blank=True, null=True)
    t03ciudad = models.IntegerField(blank=True, null=True)
    t03sector = models.IntegerField(blank=True, null=True)
    t03email = models.CharField(max_length=250, blank=True, null=True)
    t03nombrecomercial = models.TextField(blank=True, null=True)
    t03fax = models.CharField(max_length=150, blank=True, null=True)
    t03celular = models.CharField(max_length=200, blank=True, null=True)
    t03fechaconstitucion = models.DateField(blank=True, null=True)
    t03actividad = models.IntegerField(blank=True, null=True)
    t03regimen = models.IntegerField(blank=True, null=True)
    t03cupo = models.FloatField(blank=True, null=True)
    t03categoria = models.IntegerField(blank=True, null=True)
    t03plazo = models.IntegerField(blank=True, null=True)
    t03formapago = models.IntegerField(blank=True, null=True)
    t03descuento = models.FloatField(blank=True, null=True)
    t03dv = models.IntegerField(blank=True, null=True)
    t03accesoips = models.NullBooleanField()
    t03enviocorreo = models.NullBooleanField()
    t03certificado = models.NullBooleanField()
    t03conceptos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t03contactotesoreria = models.CharField(max_length=250, blank=True, null=True)
    t03telefonotesoreria = models.CharField(max_length=100, blank=True, null=True)
    t03contactorecursos = models.CharField(max_length=250, blank=True, null=True)
    t03telefonorecursos = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t03tipo = models.CharField(max_length=10, blank=True, null=True)
    t03emailtesoreria = models.CharField(max_length=200, blank=True, null=True)
    t03emailrecursos = models.CharField(max_length=200, blank=True, null=True)
    t03primerapellido = models.CharField(max_length=100, blank=True, null=True)
    t03segundonombre = models.CharField(max_length=100, blank=True, null=True)
    t03primernombre = models.CharField(max_length=100, blank=True, null=True)
    t03segundoapellido = models.CharField(max_length=100, blank=True, null=True)
    t03tipoproveedor = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't03proveedores'


class T04Empleados(models.Model):
    t04id = models.AutoField(primary_key=True)
    t04nit = models.CharField(max_length=100, blank=True, null=True)
    t04telefono = models.CharField(max_length=100, blank=True, null=True)
    t04direccion = models.CharField(max_length=250, blank=True, null=True)
    t04departamento = models.IntegerField(blank=True, null=True)
    t04ciudad = models.IntegerField(blank=True, null=True)
    t04email = models.CharField(max_length=250, blank=True, null=True)
    t04celular = models.CharField(max_length=200, blank=True, null=True)
    t04dv = models.IntegerField(blank=True, null=True)
    t04primerapellido = models.CharField(max_length=100, blank=True, null=True)
    t04segundonombre = models.CharField(max_length=100, blank=True, null=True)
    t04primernombre = models.CharField(max_length=100, blank=True, null=True)
    t04segundoapellido = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t04razonsocial = models.TextField(blank=True, null=True)
    t04registro = models.TextField(blank=True, null=True)
    t04licencia = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't04empleados'


class T05Productos(models.Model):
    t05id = models.AutoField(primary_key=True)
    t05sede = models.IntegerField(blank=True, null=True)
    t05tipo = models.IntegerField(blank=True, null=True)
    t05categoria = models.IntegerField(blank=True, null=True)
    t05subcategoria = models.IntegerField(blank=True, null=True)
    t05nombre = models.TextField(blank=True, null=True)
    t05descripcion = models.TextField(blank=True, null=True)
    t05centrodecosto = models.IntegerField(blank=True, null=True)
    t05iva = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t05precio = models.FloatField(blank=True, null=True)
    t05descuentomaximo = models.FloatField(blank=True, null=True)
    t05codigo = models.CharField(max_length=100, blank=True, null=True)
    t05distanciahorizontal = models.CharField(max_length=100, blank=True, null=True)
    t05distanciavertical = models.CharField(max_length=100, blank=True, null=True)
    t05puente = models.CharField(max_length=100, blank=True, null=True)
    t05distanciaefectiva = models.CharField(max_length=100, blank=True, null=True)
    t05distanciamecanica = models.CharField(max_length=100, blank=True, null=True)
    t05marca = models.TextField(blank=True, null=True)
    t05costo = models.FloatField(blank=True, null=True)
    t05talla = models.TextField(blank=True, null=True)
    t05color = models.TextField(blank=True, null=True)
    t05codigobarras = models.TextField(blank=True, null=True)
    t05saldoinicial = models.FloatField(blank=True, null=True)
    t05unidad = models.TextField(blank=True, null=True)
    t05referencia = models.TextField(blank=True, null=True)
    t05medida = models.TextField(blank=True, null=True)
    t05stand = models.IntegerField(blank=True, null=True)
    t05proveedor = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't05productos'


class T06Centrosdecosto(models.Model):
    t06id = models.AutoField(primary_key=True)
    t06sede = models.IntegerField(blank=True, null=True)
    t06codigo = models.CharField(max_length=50, blank=True, null=True)
    t06nombre = models.CharField(max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't06centrosdecosto'


class T07Ivas(models.Model):
    t07id = models.AutoField(primary_key=True)
    t07sede = models.IntegerField(blank=True, null=True)
    t07codigo = models.CharField(max_length=50, blank=True, null=True)
    t07iva = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't07ivas'


class T08Sedes(models.Model):
    t08id = models.AutoField(primary_key=True)
    t08codigo = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t08nombre = models.TextField(blank=True, null=True)
    t08direccion = models.TextField(blank=True, null=True)
    t08telefono = models.TextField(blank=True, null=True)
    t08email = models.TextField(blank=True, null=True)
    t08ciudad = models.IntegerField(blank=True, null=True)
    t08departamento = models.IntegerField(blank=True, null=True)
    t08nit = models.CharField(max_length=30 ,blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't08sedes'


class T09Ordenestrabajo(models.Model):
    t09id = models.AutoField(primary_key=True)
    t09sede = models.IntegerField(blank=True, null=True)
    t09fecha = models.DateField(blank=True, null=True)
    t09formula = models.IntegerField(blank=True, null=True)
    t09consecutivo = models.IntegerField(blank=True, null=True)
    t09proveedor = models.IntegerField(blank=True, null=True)
    t09paciente = models.IntegerField(blank=True, null=True)
    t09observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t09elementos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t09pagos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t09lente = models.IntegerField(blank=True, null=True)
    t09montura = models.IntegerField(blank=True, null=True)
    t09descuento = models.FloatField(blank=True, null=True)
    t09descuentolente = models.FloatField(blank=True, null=True)
    t09ubicacion = models.IntegerField(blank=True, null=True)
    t09descuentootro = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't09ordenestrabajo'


class T100Prefacturas(models.Model):
    t100id = models.AutoField(primary_key=True)
    t100usuario = models.IntegerField(blank=True, null=True)
    t100desde = models.DateField(blank=True, null=True)
    t100hasta = models.DateField(blank=True, null=True)
    t100cliente = models.IntegerField(blank=True, null=True)
    t100tipo = models.CharField(max_length=100, blank=True, null=True)
    t100servicios = models.TextField(blank=True, null=True)  # This field type is a guess.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t100factura = models.TextField(blank=True, null=True)
    t100ips = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 't100prefacturas'


class T101Detalleprefactura(models.Model):
    t101id = models.AutoField(primary_key=True)
    t101prefactura = models.IntegerField(blank=True, null=True)
    t101servicio = models.IntegerField(blank=True, null=True)
    t101cantidad = models.IntegerField(blank=True, null=True)
    t101cantidadfactura = models.IntegerField(blank=True, null=True)
    t101costo = models.FloatField(blank=True, null=True)
    t101comprobado = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't101detalleprefactura'


class T102Seguimientosfacturacion(models.Model):
    t102id = models.AutoField(primary_key=True)
    t102factura = models.IntegerField(blank=True, null=True)
    t102fecha = models.DateTimeField(blank=True, null=True)
    t102usuario = models.IntegerField(blank=True, null=True)
    t102tipo = models.CharField(max_length=50, blank=True, null=True)
    t102detalle = models.TextField(blank=True, null=True)
    t102observacion = models.TextField(blank=True, null=True)
    t102contacto = models.TextField(blank=True, null=True)
    t102cargo = models.TextField(blank=True, null=True)
    t102telefono = models.TextField(blank=True, null=True)
    t102email = models.TextField(blank=True, null=True)
    t102guia = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t102empresa = models.TextField(blank=True, null=True)
    t102telefonoempresa = models.TextField(blank=True, null=True)
    t102valorenvio = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't102seguimientosfacturacion'


class T103Preciosproveedor(models.Model):
    t103id = models.AutoField(primary_key=True)
    t103usuario = models.IntegerField(blank=True, null=True)
    t103proveedor = models.IntegerField(blank=True, null=True)
    t103examen = models.IntegerField(blank=True, null=True)
    t103valor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't103preciosproveedor'


class T104Notasdebito(models.Model):
    t104id = models.AutoField(primary_key=True)
    t104usuario = models.IntegerField(blank=True, null=True)
    t104fecha = models.DateField(blank=True, null=True)
    t104comprobante = models.IntegerField(blank=True, null=True)
    t104nuevocomprobante = models.IntegerField(blank=True, null=True)
    t104motivo = models.TextField(blank=True, null=True)
    t104observacion = models.TextField(blank=True, null=True)
    t104total = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't104notasdebito'


class T105Solicitudes(models.Model):
    t105id = models.AutoField(primary_key=True)
    t105usuario = models.IntegerField(blank=True, null=True)
    t105fecha = models.DateTimeField(blank=True, null=True)
    t105dependencia = models.IntegerField(blank=True, null=True)
    t105tipo = models.IntegerField(blank=True, null=True)
    t105encargado = models.IntegerField(blank=True, null=True)
    t105observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t105cerrada = models.NullBooleanField()
    t105vista = models.NullBooleanField()
    t105leida = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't105solicitudes'


class T106Respuestassolicitud(models.Model):
    t106id = models.AutoField(primary_key=True)
    t106solicitud = models.IntegerField(blank=True, null=True)
    t106usuario = models.IntegerField(blank=True, null=True)
    t106descripcion = models.TextField(blank=True, null=True)
    t106fecha = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t106vista = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't106respuestassolicitud'


class T107Respuestasbateria(models.Model):
    t107id = models.AutoField(primary_key=True)
    t107orden = models.IntegerField(blank=True, null=True)
    t107pregunta = models.IntegerField(blank=True, null=True)
    t107respuesta = models.IntegerField(blank=True, null=True)
    t107usuario = models.IntegerField(blank=True, null=True)
    t107grupo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't107respuestasbateria'


class T108Resultadobaterias(models.Model):
    t108id = models.AutoField(primary_key=True)
    t108orden = models.IntegerField(blank=True, null=True)
    t108usuario = models.IntegerField(blank=True, null=True)
    t108forma = models.TextField(blank=True, null=True)
    t108caracteristica_liderazgo_bruto = models.FloatField(blank=True, null=True)
    t108relaciones_sociales_trabajo_bruto = models.FloatField(blank=True, null=True)
    t108retroalimentacion_desempeno_bruto = models.FloatField(blank=True, null=True)
    t108relacion_colaboradores_bruto = models.FloatField(blank=True, null=True)
    t108total_liderazgo_bruto = models.FloatField(blank=True, null=True)
    t108claridad_del_rol_bruto = models.FloatField(blank=True, null=True)
    t108capacitacion_bruto = models.FloatField(blank=True, null=True)
    t108participacion_manejo_cambio_bruto = models.FloatField(blank=True, null=True)
    t108oportunidades_usar_habilidades_bruto = models.FloatField(blank=True, null=True)
    t108control_autonomia_trabajo_bruto = models.FloatField(blank=True, null=True)
    t108total_control_bruto = models.FloatField(blank=True, null=True)
    t108ambientales_fisico_bruto = models.FloatField(blank=True, null=True)
    t108emocionales_bruto = models.FloatField(blank=True, null=True)
    t108cuantitativas_bruto = models.FloatField(blank=True, null=True)
    t108influencia_trabajo_sobre_entorno_bruto = models.FloatField(blank=True, null=True)
    t108responsabilidad_cargo_bruto = models.FloatField(blank=True, null=True)
    t108carga_mental_bruto = models.FloatField(blank=True, null=True)
    t108consistencia_del_rol_bruto = models.FloatField(blank=True, null=True)
    t108jornada_trabajo_bruto = models.FloatField(blank=True, null=True)
    t108total_demandas_trabajo_bruto = models.FloatField(blank=True, null=True)
    t108pertenencia_organizacion_bruto = models.FloatField(blank=True, null=True)
    t108reconocimiento_compensacion_bruto = models.FloatField(blank=True, null=True)
    t108tota_recompensas_bruto = models.FloatField(blank=True, null=True)
    t108total_intralaboral_bruto = models.FloatField(blank=True, null=True)
    t108estres_pb1_bruto = models.FloatField(blank=True, null=True)
    t108estres_pb2_bruto = models.FloatField(blank=True, null=True)
    t108estres_pb3_bruto = models.FloatField(blank=True, null=True)
    t108estres_pb4_bruto = models.FloatField(blank=True, null=True)
    t108tiempo_fuera_del_trabajo_bruto = models.FloatField(blank=True, null=True)
    t108relaciones_familiares_bruto = models.FloatField(blank=True, null=True)
    t108compromiso_relaciones_interpersonales_bruto = models.FloatField(blank=True, null=True)
    t108situacion_economica_bruto = models.FloatField(blank=True, null=True)
    t108caracteristicas_vivienda_entorno_bruto = models.FloatField(blank=True, null=True)
    t108influencia_entorno_trabajo_bruto = models.FloatField(blank=True, null=True)
    t108desplazamiento_vivienda_trabajo_bruto = models.FloatField(blank=True, null=True)
    t108total_extralaboral_bruto = models.FloatField(blank=True, null=True)
    t108caracteristica_liderazgo_transformado = models.FloatField(blank=True, null=True)
    t108relaciones_sociales_trabajo_transformado = models.FloatField(blank=True, null=True)
    t108retroalimentacion_desempeno_transformado = models.FloatField(blank=True, null=True)
    t108relacion_colaboradores_transformado = models.FloatField(blank=True, null=True)
    t108total_liderazgo_transformado = models.FloatField(blank=True, null=True)
    t108claridad_del_rol_transformado = models.FloatField(blank=True, null=True)
    t108capacitacion_transformado = models.FloatField(blank=True, null=True)
    t108participacion_manejo_cambio_transformado = models.FloatField(blank=True, null=True)
    t108oportunidades_usar_habilidades_transformado = models.FloatField(blank=True, null=True)
    t108control_autonomia_trabajo_transformado = models.FloatField(blank=True, null=True)
    t108total_control_transformado = models.FloatField(blank=True, null=True)
    t108ambientales_fisico_transformado = models.FloatField(blank=True, null=True)
    t108emocionales_transformado = models.FloatField(blank=True, null=True)
    t108cuantitativas_transformado = models.FloatField(blank=True, null=True)
    t108influencia_trabajo_sobre_entorno_transformado = models.FloatField(blank=True, null=True)
    t108responsabilidad_cargo_transformado = models.FloatField(blank=True, null=True)
    t108carga_mental_transformado = models.FloatField(blank=True, null=True)
    t108consistencia_del_rol_transformado = models.FloatField(blank=True, null=True)
    t108jornada_trabajo_transformado = models.FloatField(blank=True, null=True)
    t108total_demandas_trabajo_transformado = models.FloatField(blank=True, null=True)
    t108pertenencia_organizacion_transformado = models.FloatField(blank=True, null=True)
    t108reconocimiento_compensacion_transformado = models.FloatField(blank=True, null=True)
    t108tota_recompensas_transformado = models.FloatField(blank=True, null=True)
    t108total_intralaboral_transformado = models.FloatField(blank=True, null=True)
    t108estres_pb1_transformado = models.FloatField(blank=True, null=True)
    t108estres_pb2_transformado = models.FloatField(blank=True, null=True)
    t108estres_pb3_transformado = models.FloatField(blank=True, null=True)
    t108estres_pb4_transformado = models.FloatField(blank=True, null=True)
    t108estres_total_transformado = models.FloatField(blank=True, null=True)
    t108tiempo_fuera_del_trabajo_transformado = models.FloatField(blank=True, null=True)
    t108relaciones_familiares_transformado = models.FloatField(blank=True, null=True)
    t108compromiso_relaciones_interpersonales_transformado = models.FloatField(blank=True, null=True)
    t108situacion_economica_transformado = models.FloatField(blank=True, null=True)
    t108caracteristicas_vivienda_entorno_transformado = models.FloatField(blank=True, null=True)
    t108influencia_entorno_trabajo_transformado = models.FloatField(blank=True, null=True)
    t108desplazamiento_vivienda_trabajo_transformado = models.FloatField(blank=True, null=True)
    t108total_extralaboral_transformado = models.FloatField(blank=True, null=True)
    t108total_intralaboral_extralaboral = models.FloatField(blank=True, null=True)
    t108updated_at = models.DateTimeField(blank=True, null=True)
    t108created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't108resultadobaterias'


class T10Formulas(models.Model):
    t10id = models.AutoField(primary_key=True)
    t10sede = models.IntegerField(blank=True, null=True)
    t10fecha = models.DateField(blank=True, null=True)
    t10consecutivo = models.IntegerField(blank=True, null=True)
    t10paciente = models.IntegerField(blank=True, null=True)
    t10odesf = models.CharField(max_length=50, blank=True, null=True)
    t10odcil = models.CharField(max_length=50, blank=True, null=True)
    t10odeje = models.CharField(max_length=50, blank=True, null=True)
    t10odadd = models.CharField(max_length=50, blank=True, null=True)
    t10oddp = models.CharField(max_length=50, blank=True, null=True)
    t10odalt = models.CharField(max_length=50, blank=True, null=True)
    t10odprisma = models.CharField(max_length=50, blank=True, null=True)
    t10odcurva = models.CharField(max_length=50, blank=True, null=True)
    t10odpoder = models.CharField(max_length=50, blank=True, null=True)
    t10oddiametro = models.CharField(max_length=50, blank=True, null=True)
    t10oiesf = models.CharField(max_length=50, blank=True, null=True)
    t10oicil = models.CharField(max_length=50, blank=True, null=True)
    t10oieje = models.CharField(max_length=50, blank=True, null=True)
    t10oiadd = models.CharField(max_length=50, blank=True, null=True)
    t10oidp = models.CharField(max_length=50, blank=True, null=True)
    t10oialt = models.CharField(max_length=50, blank=True, null=True)
    t10oiprisma = models.CharField(max_length=50, blank=True, null=True)
    t10oicurva = models.CharField(max_length=50, blank=True, null=True)
    t10oipoder = models.CharField(max_length=50, blank=True, null=True)
    t10oidiametro = models.CharField(max_length=50, blank=True, null=True)
    t10lente = models.IntegerField(blank=True, null=True)
    t10montura = models.IntegerField(blank=True, null=True)
    t10observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t10lenteesf = models.CharField(max_length=50, blank=True, null=True)
    t10lentecil = models.CharField(max_length=50, blank=True, null=True)
    t10lenteeje = models.CharField(max_length=50, blank=True, null=True)
    t10lenteadd = models.CharField(max_length=50, blank=True, null=True)
    t10lentedp = models.CharField(max_length=50, blank=True, null=True)
    t10lentealt = models.CharField(max_length=50, blank=True, null=True)
    t10lenteprisma = models.CharField(max_length=50, blank=True, null=True)
    t10lentecurva = models.CharField(max_length=50, blank=True, null=True)
    t10lentepoder = models.CharField(max_length=50, blank=True, null=True)
    t10lentediametro = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't10formulas'


class T110Armasmotricidad(models.Model):
    t110id = models.AutoField(primary_key=True)
    t110orden = models.IntegerField(blank=True, null=True)
    t110fecha = models.DateField(blank=True, null=True)
    t110usuario = models.IntegerField(blank=True, null=True)
    t110atencionacierto = models.CharField(max_length=30 ,blank=True, null=True)
    t110atencionrespuesta = models.CharField(max_length=30 ,blank=True, null=True)
    t110atencionerrores = models.CharField(max_length=30 ,blank=True, null=True)
    t110reaccionesrespuesta = models.CharField(max_length=30 ,blank=True, null=True)
    t110reaccionesacierto = models.CharField(max_length=30 ,blank=True, null=True)
    t110reaccioneserrores = models.CharField(max_length=30 ,blank=True, null=True)
    t110reaccion = models.CharField(max_length=30 ,blank=True, null=True)
    t110coordinacionerror = models.CharField(max_length=30 ,blank=True, null=True)
    t110coordinacionerrores = models.CharField(max_length=30 ,blank=True, null=True)
    t110percepcion = models.CharField(max_length=30 ,blank=True, null=True)
    t110estado = models.IntegerField(blank=True, null=True)
    t110resultado = models.TextField(blank=True, null=True)
    t110restricciones = models.TextField(blank=True, null=True)
    t110observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't110armasmotricidad'


class T110FormularioSociodemograficoBateria(models.Model):
    t110orden = models.IntegerField(blank=True, null=True)
    t110id = models.AutoField(primary_key=True)
    t110usuario = models.IntegerField(blank=True, null=True)
    t110respuesta = models.TextField(blank=True, null=True)
    t110pregunta = models.IntegerField(blank=True, null=True)
    t110updated_at = models.DateTimeField(blank=True, null=True)
    t110created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't110formulario_sociodemografico_bateria'


class T111Armaspsicologia(models.Model):
    t111id = models.AutoField(primary_key=True)
    t111orden = models.IntegerField(blank=True, null=True)
    t111fecha = models.DateField(blank=True, null=True)
    t111usuario = models.IntegerField(blank=True, null=True)
    t111personalidad = models.CharField(max_length=30 ,blank=True, null=True)
    t111sustancias = models.CharField(max_length=30 ,blank=True, null=True)
    t111inteligencia = models.CharField(max_length=30 ,blank=True, null=True)
    t111estado = models.IntegerField(blank=True, null=True)
    t111resultado = models.TextField(blank=True, null=True)
    t111restricciones = models.TextField(blank=True, null=True)
    t111observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't111armaspsicologia'


class T112Conciliaciones(models.Model):
    t112id = models.AutoField(primary_key=True)
    t112usuario = models.IntegerField(blank=True, null=True)
    t112desde = models.DateField(blank=True, null=True)
    t112hasta = models.DateField(blank=True, null=True)
    t112cliente = models.IntegerField(blank=True, null=True)
    t112tipo = models.CharField(max_length=100, blank=True, null=True)
    t112servicios = models.TextField(blank=True, null=True)  # This field type is a guess.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t112factura = models.TextField(blank=True, null=True)
    t112proveedor = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't112conciliaciones'


class T113Detalleconciliacion(models.Model):
    t113id = models.AutoField(primary_key=True)
    t113conciliacion = models.IntegerField(blank=True, null=True)
    t113servicio = models.IntegerField(blank=True, null=True)
    t113cantidad = models.IntegerField(blank=True, null=True)
    t113cantidadfactura = models.IntegerField(blank=True, null=True)
    t113costo = models.FloatField(blank=True, null=True)
    t113comprobado = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't113detalleconciliacion'


class T114Ubicacionesoptica(models.Model):
    t114id = models.AutoField(primary_key=True)
    t114nombre = models.TextField(blank=True, null=True)
    t114ocupada = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t114sede = models.IntegerField(blank=True, null=True)
    t114tipo = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't114ubicacionesoptica'


class T115Plansepare(models.Model):
    t115id = models.AutoField(primary_key=True)
    t115fecha = models.DateField(blank=True, null=True)
    t115usuario = models.IntegerField(blank=True, null=True)
    t115paciente = models.IntegerField(blank=True, null=True)
    t115elementos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t115abono = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t115montura = models.IntegerField(blank=True, null=True)
    t115lente = models.IntegerField(blank=True, null=True)
    t115descuento = models.FloatField(blank=True, null=True)
    t115descuentolente = models.FloatField(blank=True, null=True)
    t115ubicacion = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't115plansepare'


class T116Abonosplansepare(models.Model):
    t116id = models.AutoField(primary_key=True)
    t116plansepare = models.IntegerField(blank=True, null=True)
    t116fecha = models.DateField(blank=True, null=True)
    t116valor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t116observacion = models.TextField(blank=True, null=True)
    t116usuario = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't116abonosplansepare'


class T117Ajustesinventario(models.Model):
    t117id = models.AutoField(primary_key=True)
    t117fecha = models.DateField(blank=True, null=True)
    t117usuario = models.IntegerField(blank=True, null=True)
    t117elementos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t117observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t117stand = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't117ajustesinventario'


class T118Stands(models.Model):
    t118id = models.AutoField(primary_key=True)
    t118nombre = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't118stands'


class T119Solicitudesmodificacioncomprobante(models.Model):
    t119id = models.AutoField(primary_key=True)
    t119usuario = models.IntegerField(blank=True, null=True)
    t119fecha = models.DateField(blank=True, null=True)
    t119comprobante = models.IntegerField(blank=True, null=True)
    t119observacion = models.TextField(blank=True, null=True)
    t119autorizo = models.IntegerField(blank=True, null=True)
    t119autorizada = models.NullBooleanField()
    t119observacionautorizada = models.TextField(blank=True, null=True)
    t119sede = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't119solicitudesmodificacioncomprobante'


class T11Pacientes(models.Model):
    t11id = models.AutoField(primary_key=True)
    t11nit = models.CharField(unique=True, max_length=100, blank=True, null=True)
    t11primernombre = models.CharField(max_length=200, blank=True, null=True)
    t11segundonombre = models.CharField(max_length=200, blank=True, null=True)
    t11primerapellido = models.CharField(max_length=200, blank=True, null=True)
    t11segundoapellido = models.CharField(max_length=200, blank=True, null=True)
    t11departamento = models.IntegerField(blank=True, null=True)
    t11ciudad = models.IntegerField(blank=True, null=True)
    t11direccion = models.TextField(blank=True, null=True)
    t11telefono = models.TextField(blank=True, null=True)
    t11email = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t11tipodocumento = models.IntegerField(blank=True, null=True)
    t11sexo = models.CharField(max_length=2, blank=True, null=True)
    t11fechanacimiento = models.DateField(blank=True, null=True)
    t11rh = models.IntegerField(blank=True, null=True)
    t11ciudadexpedicion = models.IntegerField(blank=True, null=True)
    t11ciudadnacimiento = models.IntegerField(blank=True, null=True)
    t11razonsocial = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't11pacientes'


class T120Valoracionesvoz(models.Model):
    t120id = models.AutoField(primary_key=True)
    t120fecha = models.DateField(blank=True, null=True)
    t120usuario = models.IntegerField(blank=True, null=True)
    t120orden = models.IntegerField(blank=True, null=True)
    t120horasdia = models.FloatField(blank=True, null=True)
    t120horasuso = models.FloatField(blank=True, null=True)
    t120actividades = models.TextField(blank=True, null=True)
    t120ventilacion = models.NullBooleanField()
    t120ruido = models.NullBooleanField()
    t120humos = models.NullBooleanField()
    t120temperatura = models.NullBooleanField()
    t120particulado = models.NullBooleanField()
    t120respiratoria = models.NullBooleanField()
    t120gastrica = models.NullBooleanField()
    t120hormonal = models.NullBooleanField()
    t120otologica = models.NullBooleanField()
    t120nerviosa = models.NullBooleanField()
    t120laringea = models.NullBooleanField()
    t120odonto = models.NullBooleanField()
    t120fumar = models.NullBooleanField()
    t120dolor = models.NullBooleanField()
    t120tos = models.NullBooleanField()
    t120carraspera = models.NullBooleanField()
    t120disfonia = models.NullBooleanField()
    t120sensacion = models.NullBooleanField()
    t120afonia = models.NullBooleanField()
    t120fonastenia = models.NullBooleanField()
    t120secreciones = models.NullBooleanField()
    t120disfagia = models.NullBooleanField()
    t120observaciones = models.TextField(blank=True, null=True)
    t120rendimientoa = models.TextField(blank=True, null=True)
    t120rendimientoe = models.TextField(blank=True, null=True)
    t120rendimientoo = models.TextField(blank=True, null=True)
    t120rendimientoi = models.TextField(blank=True, null=True)
    t120rendimientou = models.TextField(blank=True, null=True)
    t120soplocerrado = models.FloatField(blank=True, null=True)
    t120soploabierto = models.FloatField(blank=True, null=True)
    t120frecuencia = models.FloatField(blank=True, null=True)
    t120tiempo = models.FloatField(blank=True, null=True)
    t120glatzer = models.TextField(blank=True, null=True)
    t120coordinacion = models.TextField(blank=True, null=True)
    t120modo = models.CharField(max_length=100, blank=True, null=True)
    t120tipo = models.CharField(max_length=100, blank=True, null=True)
    t120prueba = models.CharField(max_length=100, blank=True, null=True)
    t120tono = models.CharField(max_length=100, blank=True, null=True)
    t120intensidad = models.TextField(blank=True, null=True)
    t120timbre = models.TextField(blank=True, null=True)
    t120resonancia = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t120resultado = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't120valoracionesvoz'


class T121Contratos(models.Model):
    t121id = models.AutoField(primary_key=True)
    t121cliente = models.IntegerField(blank=True, null=True)
    t121numero = models.CharField(max_length=100, blank=True, null=True)
    t121detalle = models.TextField(blank=True, null=True)
    t121fecha = models.DateField(blank=True, null=True)
    t121desde = models.DateField(blank=True, null=True)
    t121hasta = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't121contratos'


class T122Detallecontrato(models.Model):
    t122id = models.AutoField(primary_key=True)
    t122contrato = models.IntegerField(blank=True, null=True)
    t122servicio = models.IntegerField(blank=True, null=True)
    t122cantidad = models.IntegerField(blank=True, null=True)
    t122valorunitario = models.FloatField(blank=True, null=True)
    t122valortotal = models.FloatField(blank=True, null=True)
    t122ejecutado = models.FloatField(blank=True, null=True)
    t122ejecutadovalor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't122detallecontrato'


class T123Calidad(models.Model):
    t123id = models.AutoField(primary_key=True)
    t123remision = models.IntegerField(blank=True, null=True)
    t123observaciones = models.TextField(blank=True, null=True)
    t123gafas = models.TextField(blank=True, null=True)
    t123lentes = models.TextField(blank=True, null=True)
    t123montura = models.TextField(blank=True, null=True)
    t123entrega = models.TextField(blank=True, null=True)
    t123factura = models.TextField(blank=True, null=True)
    t123entrada = models.TextField(blank=True, null=True)
    t123invima = models.TextField(blank=True, null=True)
    t123lote = models.TextField(blank=True, null=True)
    t123usuario = models.IntegerField(blank=True, null=True)
    t123fecha = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't123calidad'


class T124Trasladosinventario(models.Model):
    t124id = models.AutoField(primary_key=True)
    t124fecha = models.DateField(blank=True, null=True)
    t124usuario = models.IntegerField(blank=True, null=True)
    t124elementos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t124observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t124stand = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't124trasladosinventario'


class T125Sesionescaja(models.Model):
    t125id = models.AutoField(primary_key=True)
    t125usuario = models.IntegerField(blank=True, null=True)
    t125usuariocierre = models.IntegerField(blank=True, null=True)
    t125apertura = models.DateTimeField(blank=True, null=True)
    t125cierre = models.DateTimeField(blank=True, null=True)
    t125base = models.FloatField(blank=True, null=True)
    t125arqueo = models.FloatField(blank=True, null=True)
    t125observaciones = models.TextField(blank=True, null=True)
    t125elementos = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't125sesionescaja'


class T12Pagosordendetrabajo(models.Model):
    t12id = models.AutoField(primary_key=True)
    t12ordendetrabajo = models.IntegerField(blank=True, null=True)
    t12sede = models.IntegerField(blank=True, null=True)
    t12formapago = models.IntegerField(blank=True, null=True)
    t12valor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't12pagosordendetrabajo'


class T135ParticipantesEventos(models.Model):
    t135cedula = models.TextField(blank=True, null=True)
    t135como_se_entero = models.TextField(blank=True, null=True)
    t135direccion = models.TextField(blank=True, null=True)
    t135email = models.TextField(blank=True, null=True)
    t135empresa = models.TextField(blank=True, null=True)
    t135id = models.AutoField(primary_key=True)
    t135nit = models.TextField(blank=True, null=True)
    t135nombre = models.TextField(blank=True, null=True)
    t135updated_at = models.DateTimeField(blank=True, null=True)
    t135created_at = models.DateTimeField(blank=True, null=True)
    t135telefono = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't135participantes_eventos'


class T136ConfirmacionAsistencia(models.Model):
    t136id = models.AutoField(primary_key=True)
    t136participante = models.IntegerField(blank=True, null=True)
    t136updated_at = models.DateTimeField(blank=True, null=True)
    t136created_at = models.DateTimeField(blank=True, null=True)
    t136entrada = models.NullBooleanField()
    t136asistencia = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't136confirmacion_asistencia'


class T13Remisiones(models.Model):
    t13id = models.AutoField(primary_key=True)
    t13sede = models.IntegerField(blank=True, null=True)
    t13fecha = models.DateField(blank=True, null=True)
    t13ordendetrabajo = models.IntegerField(blank=True, null=True)
    t13consecutivo = models.IntegerField(blank=True, null=True)
    t13observaciones = models.TextField(blank=True, null=True)
    t13fechaentrega = models.DateField(blank=True, null=True)
    t13horaentrega = models.TimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t13proveedor = models.IntegerField(blank=True, null=True)
    t13factura = models.CharField(max_length=100, blank=True, null=True)
    t13recibida = models.NullBooleanField()
    t13calidad = models.NullBooleanField()
    t13observacionrecibida = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't13remisiones'


class T14Prefijos(models.Model):
    t14id = models.AutoField(primary_key=True)
    t14sede = models.IntegerField(blank=True, null=True)
    t14codigo = models.CharField(max_length=30 ,blank=True, null=True)
    t14desde = models.IntegerField(blank=True, null=True)
    t14hasta = models.IntegerField(blank=True, null=True)
    t14resolucion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't14prefijos'


class T15Ventas(models.Model):
    t15id = models.AutoField(primary_key=True)
    t15sede = models.IntegerField(blank=True, null=True)
    t15prefijo = models.IntegerField(blank=True, null=True)
    t15consecutivo = models.IntegerField(blank=True, null=True)
    t15fecha = models.DateField(blank=True, null=True)
    t15subtotal = models.FloatField(blank=True, null=True)
    t15iva = models.FloatField(blank=True, null=True)
    t15total = models.FloatField(blank=True, null=True)
    t15observaciones = models.TextField(blank=True, null=True)
    t15paciente = models.IntegerField(blank=True, null=True)
    t15cliente = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t15ordendetrabajo = models.IntegerField(blank=True, null=True)
    t15tipo = models.CharField(max_length=30 ,blank=True, null=True)
    t15elementos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t15pagos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t15descuento = models.FloatField(blank=True, null=True)
    t15descuentolente = models.FloatField(blank=True, null=True)
    t15descuentootro = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't15ventas'


class T16Detalleventa(models.Model):
    t16id = models.AutoField(primary_key=True)
    t16sede = models.IntegerField(blank=True, null=True)
    t16venta = models.IntegerField(blank=True, null=True)
    t16producto = models.IntegerField(blank=True, null=True)
    t16servicio = models.IntegerField(blank=True, null=True)
    t16valor = models.FloatField(blank=True, null=True)
    t16iva = models.FloatField(blank=True, null=True)
    t16cantidad = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't16detalleventa'


class T17Kardex(models.Model):
    t17id = models.AutoField(primary_key=True)
    t17sede = models.IntegerField(blank=True, null=True)
    t17producto = models.IntegerField(blank=True, null=True)
    t17servicio = models.IntegerField(blank=True, null=True)
    t17cantidad = models.IntegerField(blank=True, null=True)
    t17valor = models.FloatField(blank=True, null=True)
    t17iva = models.FloatField(blank=True, null=True)
    t17venta = models.IntegerField(blank=True, null=True)
    t17compra = models.IntegerField(blank=True, null=True)
    t17saldo = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t17abono = models.IntegerField(blank=True, null=True)
    t17ordentrabajo = models.IntegerField(blank=True, null=True)
    t17ajuste = models.IntegerField(blank=True, null=True)
    t17fecha = models.DateField(blank=True, null=True)
    t17stand = models.IntegerField(blank=True, null=True)
    t17costo = models.FloatField(blank=True, null=True)
    t17traslado = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't17kardex'


class T18Pagosventa(models.Model):
    t18id = models.AutoField(primary_key=True)
    t18sede = models.IntegerField(blank=True, null=True)
    t18venta = models.IntegerField(blank=True, null=True)
    t18formapago = models.IntegerField(blank=True, null=True)
    t18valor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't18pagosventa'


class T19Turnos(models.Model):
    t19id = models.AutoField(primary_key=True)
    t19sede = models.IntegerField(blank=True, null=True)
    t19usuario = models.IntegerField(blank=True, null=True)
    t19fecha = models.DateTimeField(blank=True, null=True)
    t19tipo = models.CharField(max_length=20, blank=True, null=True)
    t19turno = models.IntegerField(blank=True, null=True)
    t19prioridad = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t19paciente = models.CharField(max_length=50, blank=True, null=True)
    t19activo = models.NullBooleanField()
    t19ultimaactividad = models.DateTimeField(blank=True, null=True)
    t19agendando = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't19turnos'


class T20Consultorios(models.Model):
    t20id = models.AutoField(primary_key=True)
    t20sede = models.IntegerField(blank=True, null=True)
    t20nombre = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t20activos = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 't20consultorios'


class T21Historialturnos(models.Model):
    t21id = models.AutoField(primary_key=True)
    t21turno = models.IntegerField(blank=True, null=True)
    t21fecha = models.DateTimeField(blank=True, null=True)
    t21usuario = models.IntegerField(blank=True, null=True)
    t21tipo = models.CharField(max_length=20, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t21consultorio = models.IntegerField(blank=True, null=True)
    t21orden = models.IntegerField(blank=True, null=True)
    t21sede = models.IntegerField(blank=True, null=True)
    t21activa = models.NullBooleanField()
    t21preorden = models.IntegerField(blank=True, null=True)
    t21activa2 = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't21historialturnos'


class T22Ordenes(models.Model):
    t22id = models.AutoField(primary_key=True)
    t22consecutivo = models.IntegerField(blank=True, null=True)
    t22sede = models.IntegerField(blank=True, null=True)
    t22cliente = models.IntegerField(blank=True, null=True)
    t22entidad = models.IntegerField(blank=True, null=True)
    t22fecha = models.DateField(blank=True, null=True)
    t22tipo = models.IntegerField(blank=True, null=True)
    t22orden = models.CharField(max_length=200, blank=True, null=True)
    t22extramural = models.NullBooleanField()
    t22rednacional = models.NullBooleanField()
    t22paciente = models.IntegerField(blank=True, null=True)
    t22eps = models.IntegerField(blank=True, null=True)
    t22arl = models.IntegerField(blank=True, null=True)
    t22pension = models.IntegerField(blank=True, null=True)
    t22raza = models.IntegerField(blank=True, null=True)
    t22estadocivil = models.IntegerField(blank=True, null=True)
    t22niveleducativo = models.IntegerField(blank=True, null=True)
    t22hijos = models.IntegerField(blank=True, null=True)
    t22ingresos = models.IntegerField(blank=True, null=True)
    t22nucleo = models.IntegerField(blank=True, null=True)
    t22estrato = models.IntegerField(blank=True, null=True)
    t22profesion = models.IntegerField(blank=True, null=True)
    t22telefono = models.CharField(max_length=50, blank=True, null=True)
    t22email = models.CharField(max_length=200, blank=True, null=True)
    t22ciudadresidencia = models.IntegerField(blank=True, null=True)
    t22ciudadexamen = models.IntegerField(blank=True, null=True)
    t22direccion = models.CharField(max_length=200, blank=True, null=True)
    t22localidad = models.IntegerField(blank=True, null=True)
    t22barrio = models.CharField(max_length=100, blank=True, null=True)
    t22zona = models.IntegerField(blank=True, null=True)
    t22nombreacompanante = models.CharField(max_length=200, blank=True, null=True)
    t22telefonoacompanante = models.CharField(max_length=50, blank=True, null=True)
    t22nombreresponsable = models.CharField(max_length=200, blank=True, null=True)
    t22parentescoresponsable = models.CharField(max_length=50, blank=True, null=True)
    t22telefonoresponsable = models.CharField(max_length=50, blank=True, null=True)
    t22fechaingreso = models.DateField(blank=True, null=True)
    t22jornada = models.IntegerField(blank=True, null=True)
    t22cargo = models.TextField(blank=True, null=True)
    t22area = models.CharField(max_length=100, blank=True, null=True)
    t22antiguedad = models.CharField(max_length=100, blank=True, null=True)
    t22funciones = models.TextField(blank=True, null=True)
    t22maquinaria = models.TextField(blank=True, null=True)
    t22herramientas = models.TextField(blank=True, null=True)
    t22materiaprima = models.TextField(blank=True, null=True)
    t22padre = models.IntegerField(blank=True, null=True)
    t22preorden = models.IntegerField(blank=True, null=True)
    t22estado = models.IntegerField(blank=True, null=True)
    t22observacion = models.TextField(blank=True, null=True)
    t22detalleemail = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t22turno = models.IntegerField(blank=True, null=True)
    t22examenes = models.TextField(blank=True, null=True)
    t22consulta = models.NullBooleanField()
    t22terminado = models.DateTimeField(blank=True, null=True)
    t22finalizado = models.NullBooleanField()
    t22contacto = models.TextField(blank=True, null=True)
    t22telefonocontacto = models.TextField(blank=True, null=True)
    t22emailcontacto = models.TextField(blank=True, null=True)
    t22dividir = models.NullBooleanField()
    t22tipo1 = models.CharField(max_length=50, blank=True, null=True)
    t22facturar1 = models.CharField(max_length=50, blank=True, null=True)
    t22tercero1 = models.IntegerField(blank=True, null=True)
    t22tipo2 = models.CharField(max_length=50, blank=True, null=True)
    t22facturar2 = models.CharField(max_length=50, blank=True, null=True)
    t22tercero2 = models.IntegerField(blank=True, null=True)
    t22usuario = models.IntegerField(blank=True, null=True)
    t22prioridad = models.IntegerField(blank=True, null=True)
    t22modificable = models.NullBooleanField()
    t22ips = models.IntegerField(blank=True, null=True)
    t22fecharecibido = models.DateField(blank=True, null=True)
    t22horarecibido = models.TimeField(blank=True, null=True)
    t22confirmada = models.NullBooleanField()
    t22centrodecosto = models.IntegerField(blank=True, null=True)
    t22unidadgestion = models.IntegerField(blank=True, null=True)
    t22restricciones = models.TextField(blank=True, null=True)  # This field type is a guess.
    t22observaciones = models.TextField(blank=True, null=True)
    t22cantidadexamenes = models.IntegerField(blank=True, null=True)
    t22oculto = models.NullBooleanField()
    t22codigounico = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't22ordenes'


class T23Examenesorden(models.Model):
    t23id = models.AutoField(primary_key=True)
    t23orden = models.IntegerField(blank=True, null=True)
    t23examen = models.IntegerField(blank=True, null=True)
    t23usuario = models.IntegerField(blank=True, null=True)
    t23realizo = models.IntegerField(blank=True, null=True)
    t23estado = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't23examenesorden'


class T24Examenes(models.Model):
    t24id = models.AutoField(primary_key=True)
    t24codigo = models.CharField(max_length=50, blank=True, null=True)
    t24nombre = models.CharField(max_length=200, blank=True, null=True)
    t24tipo = models.IntegerField(blank=True, null=True)
    t24tipousuario = models.IntegerField(blank=True, null=True)
    t24dependencia = models.IntegerField(blank=True, null=True)
    t24principal = models.NullBooleanField()
    t24formulario = models.CharField(max_length=50, blank=True, null=True)
    t24reporte = models.NullBooleanField()
    t24ocultar = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t24observacion = models.TextField(blank=True, null=True)
    t24necesariocierre = models.NullBooleanField()
    t24necesarios = models.TextField(blank=True, null=True)  # This field type is a guess.
    t24rips = models.NullBooleanField()
    t24tercerizar = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't24examenes'


class T25Servicios(models.Model):
    t25id = models.AutoField(primary_key=True)
    t25codigo = models.CharField(max_length=50, blank=True, null=True)
    t25nombre = models.CharField(max_length=200, blank=True, null=True)
    t25examenes = models.TextField(blank=True, null=True)  # This field type is a guess.
    t25iva = models.IntegerField(blank=True, null=True)
    t25centrodecosto = models.IntegerField(blank=True, null=True)
    t25requeridos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t25precio = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t25activo = models.NullBooleanField()
    t25nodian = models.NullBooleanField()
    t25requeridos2 = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't25servicios'


class T26Precios(models.Model):
    t26id = models.AutoField(primary_key=True)
    t26cliente = models.IntegerField(blank=True, null=True)
    t26ips = models.IntegerField(blank=True, null=True)
    t26extramural = models.NullBooleanField()
    t26servicio = models.IntegerField(blank=True, null=True)
    t26precio = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t26sede = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't26precios'


class T27Examenesusuario(models.Model):
    t27id = models.AutoField(primary_key=True)
    t27examen = models.IntegerField(blank=True, null=True)
    t27usuario = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't27examenesusuario'


class T28Examenmedico(models.Model):
    t28id = models.AutoField(primary_key=True)
    t28orden = models.IntegerField(blank=True, null=True)
    t28usuario = models.IntegerField(blank=True, null=True)
    t28fecha = models.DateTimeField(blank=True, null=True)
    t28fechacierre = models.DateTimeField(blank=True, null=True)
    t28usuariocierre = models.IntegerField(blank=True, null=True)
    t28epp = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28otroepp = models.TextField(blank=True, null=True)
    t28riesgosfisicos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28riesgosbiologicos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28riesgosquimicos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28riesgosergonomicos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28psicolaborales = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28riesgosseguridad = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28riesgosotros = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28observacionrevision = models.TextField(blank=True, null=True)
    t28ejercicio = models.NullBooleanField()
    t28ejerciciocuales = models.TextField(blank=True, null=True)
    t28ejerciciohoras = models.TextField(blank=True, null=True)
    t28deportes = models.NullBooleanField()
    t28deportescuales = models.TextField(blank=True, null=True)
    t28deporteshoras = models.TextField(blank=True, null=True)
    t28manuales = models.NullBooleanField()
    t28manualescuales = models.TextField(blank=True, null=True)
    t28manualeshoras = models.TextField(blank=True, null=True)
    t28oficios = models.NullBooleanField()
    t28oficioscuales = models.TextField(blank=True, null=True)
    t28oficioshoras = models.TextField(blank=True, null=True)
    t28habitostoxicos = models.NullBooleanField()
    t28alcohol = models.TextField(blank=True, null=True)
    t28frecuenciaalcohol = models.TextField(blank=True, null=True)
    t28alcoholanios = models.TextField(blank=True, null=True)
    t28cigarrillo = models.TextField(blank=True, null=True)
    t28cigariilofrecuencia = models.TextField(blank=True, null=True)
    t28cigarrilloanios = models.TextField(blank=True, null=True)
    t28otrassutancias = models.TextField(blank=True, null=True)
    t28otrassutanciasfrecuencia = models.TextField(blank=True, null=True)
    t28otrassutanciasanios = models.TextField(blank=True, null=True)
    t28tension = models.TextField(blank=True, null=True)
    t28frecuencia = models.FloatField(blank=True, null=True)
    t28frecuenciarespiratoria = models.FloatField(blank=True, null=True)
    t28lateraldominante = models.CharField(max_length=50, blank=True, null=True)
    t28talla = models.FloatField(blank=True, null=True)
    t28peso = models.FloatField(blank=True, null=True)
    t28imc = models.FloatField(blank=True, null=True)
    t28interpretacionimc = models.CharField(max_length=50, blank=True, null=True)
    t28perimetroabdominal = models.FloatField(blank=True, null=True)
    t28interpretacionperimetroabdominal = models.CharField(max_length=50, blank=True, null=True)
    t28observaciones = models.TextField(blank=True, null=True)
    t28resultado = models.CharField(max_length=100, blank=True, null=True)
    t28continuaensucargo = models.NullBooleanField()
    t28reubicado = models.NullBooleanField()
    t28arp = models.NullBooleanField()
    t28espaciosconfinados = models.CharField(max_length=50, blank=True, null=True)
    t28recomendacionescargo = models.TextField(blank=True, null=True)
    t28recomendacionesnocargo = models.TextField(blank=True, null=True)
    t28valoracioneps = models.NullBooleanField()
    t28formatoeps = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t28revision = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28recomendaciones = models.TextField(blank=True, null=True)  # This field type is a guess.
    t28ciclo = models.CharField(max_length=50, blank=True, null=True)
    t28fum = models.DateField(blank=True, null=True)
    t28gestaciones = models.IntegerField(blank=True, null=True)
    t28partos = models.IntegerField(blank=True, null=True)
    t28abortos = models.IntegerField(blank=True, null=True)
    t28hijosvivos = models.IntegerField(blank=True, null=True)
    t28embarazosectopicos = models.IntegerField(blank=True, null=True)
    t28fuc = models.DateField(blank=True, null=True)
    t28resultadocitologia = models.TextField(blank=True, null=True)
    t28planificacion = models.NullBooleanField()
    t28metodoplanificacion = models.CharField(max_length=250, blank=True, null=True)
    t28alturas = models.CharField(max_length=50, blank=True, null=True)
    t28alimentos = models.CharField(max_length=50, blank=True, null=True)
    t28agroquimicos = models.CharField(max_length=50, blank=True, null=True)
    t28restriccionescliente = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 't28examenmedico'


class T29Riesgosempresa(models.Model):
    t29id = models.AutoField(primary_key=True)
    t29examen = models.IntegerField(blank=True, null=True)
    t29nombre = models.TextField(blank=True, null=True)
    t29actividad = models.TextField(blank=True, null=True)
    t29seccion = models.TextField(blank=True, null=True)
    t29cargo = models.TextField(blank=True, null=True)
    t29tiempo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t29funciones = models.TextField(blank=True, null=True)
    t29medidas = models.TextField(blank=True, null=True)
    t29soportes = models.NullBooleanField()
    t29cualessoportes = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't29riesgosempresa'


class T30Accidenteshistoria(models.Model):
    t30id = models.AutoField(primary_key=True)
    t30examen = models.IntegerField(blank=True, null=True)
    t30fecha = models.DateField(blank=True, null=True)
    t30nombre = models.TextField(blank=True, null=True)
    t30naturaleza = models.TextField(blank=True, null=True)
    t30parte = models.TextField(blank=True, null=True)
    t30dias = models.IntegerField(blank=True, null=True)
    t30arp = models.NullBooleanField()
    t30secuelas = models.TextField(blank=True, null=True)
    t30atencion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't30accidenteshistoria'


class T31Enfermedadeshistoria(models.Model):
    t31id = models.AutoField(primary_key=True)
    t31examen = models.IntegerField(blank=True, null=True)
    t31enfermedad = models.TextField(blank=True, null=True)
    t31fecha = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't31enfermedadeshistoria'


class T32Antecedentesfamiliares(models.Model):
    t32id = models.AutoField(primary_key=True)
    t32examen = models.IntegerField(blank=True, null=True)
    t32patologia = models.IntegerField(blank=True, null=True)
    t32presenta = models.NullBooleanField()
    t32parentesco = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't32antecedentesfamiliares'


class T33Antecedentespersonales(models.Model):
    t33id = models.AutoField(primary_key=True)
    t33examen = models.IntegerField(blank=True, null=True)
    t33tipo = models.IntegerField(blank=True, null=True)
    t33presenta = models.NullBooleanField()
    t33observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t33diagnostico = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't33antecedentespersonales'


class T34Inmunizacionhistoria(models.Model):
    t34id = models.AutoField(primary_key=True)
    t34examen = models.IntegerField(blank=True, null=True)
    t34tipo = models.IntegerField(blank=True, null=True)
    t34fecha = models.DateField(blank=True, null=True)
    t34dosis = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't34inmunizacionhistoria'


class T35Valoracionsistemas(models.Model):
    t35id = models.AutoField(primary_key=True)
    t35examen = models.IntegerField(blank=True, null=True)
    t35tipo = models.IntegerField(blank=True, null=True)
    t35resultado = models.TextField(blank=True, null=True)
    t35descripcion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't35valoracionsistemas'


class T36Diagnosticos(models.Model):
    t36id = models.AutoField(primary_key=True)
    t36examen = models.IntegerField(blank=True, null=True)
    t36cie10 = models.IntegerField(blank=True, null=True)
    t36tipo = models.TextField(blank=True, null=True)
    t36origen = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't36diagnosticos'


class T37Audiometrias(models.Model):
    t37id = models.AutoField(primary_key=True)
    t37orden = models.IntegerField(blank=True, null=True)
    t37fecha = models.DateTimeField(blank=True, null=True)
    t37usuario = models.IntegerField(blank=True, null=True)
    t37descanso = models.FloatField(blank=True, null=True)
    t37cabina = models.NullBooleanField()
    t37retest = models.NullBooleanField()
    t37oidoizquierdo = models.CharField(max_length=100, blank=True, null=True)
    t37oidoderecho = models.CharField(max_length=100, blank=True, null=True)
    t37oda250 = models.FloatField(blank=True, null=True)
    t37oda500 = models.FloatField(blank=True, null=True)
    t37oda1000 = models.FloatField(blank=True, null=True)
    t37oda2000 = models.FloatField(blank=True, null=True)
    t37oda3000 = models.FloatField(blank=True, null=True)
    t37oda4000 = models.FloatField(blank=True, null=True)
    t37oda6000 = models.FloatField(blank=True, null=True)
    t37oda8000 = models.FloatField(blank=True, null=True)
    t37oia250 = models.FloatField(blank=True, null=True)
    t37oia500 = models.FloatField(blank=True, null=True)
    t37oia1000 = models.FloatField(blank=True, null=True)
    t37oia2000 = models.FloatField(blank=True, null=True)
    t37oia3000 = models.FloatField(blank=True, null=True)
    t37oia4000 = models.FloatField(blank=True, null=True)
    t37oia6000 = models.FloatField(blank=True, null=True)
    t37oia8000 = models.FloatField(blank=True, null=True)
    t37odo250 = models.FloatField(blank=True, null=True)
    t37odo500 = models.FloatField(blank=True, null=True)
    t37odo1000 = models.FloatField(blank=True, null=True)
    t37odo2000 = models.FloatField(blank=True, null=True)
    t37odo3000 = models.FloatField(blank=True, null=True)
    t37odo4000 = models.FloatField(blank=True, null=True)
    t37odo6000 = models.FloatField(blank=True, null=True)
    t37odo8000 = models.FloatField(blank=True, null=True)
    t37oio250 = models.FloatField(blank=True, null=True)
    t37oio500 = models.FloatField(blank=True, null=True)
    t37oio1000 = models.FloatField(blank=True, null=True)
    t37oio2000 = models.FloatField(blank=True, null=True)
    t37oio3000 = models.FloatField(blank=True, null=True)
    t37oio4000 = models.FloatField(blank=True, null=True)
    t37oio6000 = models.FloatField(blank=True, null=True)
    t37oio8000 = models.FloatField(blank=True, null=True)
    t37audicionnormal = models.CharField(max_length=50, blank=True, null=True)
    t37hipoacusia1 = models.CharField(max_length=50, blank=True, null=True)
    t37hipoacusia2 = models.CharField(max_length=50, blank=True, null=True)
    t37hipoacusia3 = models.CharField(max_length=50, blank=True, null=True)
    t37hipoacusiaconductiva = models.CharField(max_length=50, blank=True, null=True)
    t37hipoacusiamixta = models.CharField(max_length=50, blank=True, null=True)
    t37severidadnormal = models.CharField(max_length=50, blank=True, null=True)
    t37severidadleve = models.CharField(max_length=50, blank=True, null=True)
    t37severidadmoderada = models.CharField(max_length=50, blank=True, null=True)
    t37severidadmoderadasevera = models.CharField(max_length=50, blank=True, null=True)
    t37severidadsevera = models.CharField(max_length=50, blank=True, null=True)
    t37severidadprofunda = models.CharField(max_length=50, blank=True, null=True)
    t37observaciones = models.TextField(blank=True, null=True)
    t37recomendaciones = models.TextField(blank=True, null=True)  # This field type is a guess.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t37resultado = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't37audiometrias'


class T38Diagnosticosaudiometria(models.Model):
    t38id = models.AutoField(primary_key=True)
    t38examen = models.IntegerField(blank=True, null=True)
    t38cie10 = models.IntegerField(blank=True, null=True)
    t38tipo = models.CharField(max_length=100, blank=True, null=True)
    t38origen = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't38diagnosticosaudiometria'


class T39Antecedentesauditivos(models.Model):
    t39id = models.AutoField(primary_key=True)
    t39examen = models.IntegerField(blank=True, null=True)
    t39tipo = models.IntegerField(blank=True, null=True)
    t39valor = models.NullBooleanField()
    t39observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't39antecedentesauditivos'


class T40Actividadesexposicion(models.Model):
    t40id = models.AutoField(primary_key=True)
    t40examen = models.IntegerField(blank=True, null=True)
    t40tipo = models.IntegerField(blank=True, null=True)
    t40valor = models.NullBooleanField()
    t40observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't40actividadesexposicion'


class T41Cie10(models.Model):
    t41id = models.AutoField(primary_key=True)
    t41codigo = models.CharField(max_length=50, blank=True, null=True)
    t41nombre = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't41cie10'


class T42Optometrias(models.Model):
    t42id = models.AutoField(primary_key=True)
    t42orden = models.IntegerField(blank=True, null=True)
    t42usuario = models.IntegerField(blank=True, null=True)
    t42fecha = models.DateTimeField(blank=True, null=True)
    t42antecedentes = models.TextField(blank=True, null=True)  # This field type is a guess.
    t42antecedentesoculares = models.TextField(blank=True, null=True)  # This field type is a guess.
    t42otrosantecedentes = models.TextField(blank=True, null=True)
    t42refraccion = models.CharField(max_length=50, blank=True, null=True)
    t42corregido = models.NullBooleanField()
    t42cualrefraccion = models.TextField(blank=True, null=True)
    t42tiempoevolucion = models.FloatField(blank=True, null=True)
    t42correccion = models.CharField(max_length=100, blank=True, null=True)
    t42tiempoultimoexamen = models.FloatField(blank=True, null=True)
    t42frecuenciauso = models.CharField(max_length=100, blank=True, null=True)
    t42tiempoultimaformula = models.FloatField(blank=True, null=True)
    t42signos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t42observacionsignos = models.TextField(blank=True, null=True)
    t42odvls = models.CharField(max_length=50, blank=True, null=True)
    t42odvps = models.CharField(max_length=50, blank=True, null=True)
    t42odvlc = models.CharField(max_length=50, blank=True, null=True)
    t42odvpc = models.CharField(max_length=50, blank=True, null=True)
    t42odvl = models.CharField(max_length=50, blank=True, null=True)
    t42odvp = models.CharField(max_length=50, blank=True, null=True)
    t42pinguecula = models.NullBooleanField()
    t42resequedad = models.NullBooleanField()
    t42inyeccion = models.NullBooleanField()
    t42hiperemia = models.NullBooleanField()
    t42ptosis = models.NullBooleanField()
    t42pterigio = models.NullBooleanField()
    t42secrecion = models.NullBooleanField()
    t42edema = models.NullBooleanField()
    t42blefaritis = models.NullBooleanField()
    t42covervisionlejana = models.CharField(max_length=50, blank=True, null=True)
    t42covervisionproxima = models.CharField(max_length=50, blank=True, null=True)
    t42puntoconvergencia = models.CharField(max_length=50, blank=True, null=True)
    t42estereopsisod = models.CharField(max_length=50, blank=True, null=True)
    t42estereopsisoi = models.CharField(max_length=50, blank=True, null=True)
    t42cromaticaod = models.CharField(max_length=50, blank=True, null=True)
    t42cromaticaoi = models.CharField(max_length=50, blank=True, null=True)
    t42campovisualod = models.CharField(max_length=50, blank=True, null=True)
    t42campovisualoi = models.CharField(max_length=50, blank=True, null=True)
    t42oftalmoscopiaod = models.CharField(max_length=50, blank=True, null=True)
    t42oftalmoscopiaoi = models.CharField(max_length=50, blank=True, null=True)
    t42odesf = models.CharField(max_length=50, blank=True, null=True)
    t42odcil = models.CharField(max_length=50, blank=True, null=True)
    t42odeje = models.CharField(max_length=50, blank=True, null=True)
    t42odagudeza = models.CharField(max_length=50, blank=True, null=True)
    t42oddp = models.CharField(max_length=50, blank=True, null=True)
    t42oddnp = models.CharField(max_length=50, blank=True, null=True)
    t42odaltv = models.CharField(max_length=50, blank=True, null=True)
    t42odaltf = models.CharField(max_length=50, blank=True, null=True)
    t42odprisma = models.CharField(max_length=50, blank=True, null=True)
    t42oiesf = models.CharField(max_length=50, blank=True, null=True)
    t42oicil = models.CharField(max_length=50, blank=True, null=True)
    t42oieje = models.CharField(max_length=50, blank=True, null=True)
    t42oiagudeza = models.CharField(max_length=50, blank=True, null=True)
    t42oidp = models.CharField(max_length=50, blank=True, null=True)
    t42oidnp = models.CharField(max_length=50, blank=True, null=True)
    t42oialtv = models.CharField(max_length=50, blank=True, null=True)
    t42oialtf = models.CharField(max_length=50, blank=True, null=True)
    t42oiprisma = models.CharField(max_length=50, blank=True, null=True)
    t42lenteesf = models.CharField(max_length=50, blank=True, null=True)
    t42lentecil = models.CharField(max_length=50, blank=True, null=True)
    t42lenteeje = models.CharField(max_length=50, blank=True, null=True)
    t42lenteagudeza = models.CharField(max_length=50, blank=True, null=True)
    t42lentedp = models.CharField(max_length=50, blank=True, null=True)
    t42lentednp = models.CharField(max_length=50, blank=True, null=True)
    t42lentealtv = models.CharField(max_length=50, blank=True, null=True)
    t42lentealtf = models.CharField(max_length=50, blank=True, null=True)
    t42lenteprisma = models.CharField(max_length=50, blank=True, null=True)
    t42emetropia = models.CharField(max_length=50, blank=True, null=True)
    t42miopia = models.CharField(max_length=50, blank=True, null=True)
    t42hipermetropia = models.CharField(max_length=50, blank=True, null=True)
    t42astigmatismo = models.CharField(max_length=50, blank=True, null=True)
    t42presbicia = models.CharField(max_length=50, blank=True, null=True)
    t42ambliopia = models.CharField(max_length=50, blank=True, null=True)
    t42motilidad = models.CharField(max_length=50, blank=True, null=True)
    t42convergencia = models.CharField(max_length=50, blank=True, null=True)
    t42refraccionnocorregido = models.CharField(max_length=50, blank=True, null=True)
    t42refraccionadecuadamente = models.CharField(max_length=50, blank=True, null=True)
    t42refraccioninadecuadamente = models.CharField(max_length=50, blank=True, null=True)
    t42norequiererefraccion = models.CharField(max_length=50, blank=True, null=True)
    t42recomendaciones = models.TextField(blank=True, null=True)  # This field type is a guess.
    t42observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t42observacionriesgos = models.TextField(blank=True, null=True)
    t42oivls = models.CharField(max_length=50, blank=True, null=True)
    t42oivps = models.CharField(max_length=50, blank=True, null=True)
    t42oivlc = models.CharField(max_length=50, blank=True, null=True)
    t42oivpc = models.CharField(max_length=50, blank=True, null=True)
    t42oivl = models.CharField(max_length=50, blank=True, null=True)
    t42oivp = models.CharField(max_length=50, blank=True, null=True)
    t42retinoscopiaod = models.CharField(max_length=50, blank=True, null=True)
    t42retinoscopiaoi = models.CharField(max_length=50, blank=True, null=True)
    t42resultado = models.TextField(blank=True, null=True)
    t42observacionformula = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't42optometrias'


class T43Riesgosoptometria(models.Model):
    t43id = models.AutoField(primary_key=True)
    t43optometria = models.IntegerField(blank=True, null=True)
    t43tipo = models.IntegerField(blank=True, null=True)
    t43valor = models.NullBooleanField()
    t43tiempoanios = models.FloatField(blank=True, null=True)
    t43tiempohoras = models.FloatField(blank=True, null=True)
    t43proteccion = models.NullBooleanField()
    t43tipoproteccion = models.CharField(max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't43riesgosoptometria'


class T44Diagnosticosoptometria(models.Model):
    t44id = models.AutoField(primary_key=True)
    t44examen = models.IntegerField(blank=True, null=True)
    t44cie10 = models.IntegerField(blank=True, null=True)
    t44tipo = models.CharField(max_length=100, blank=True, null=True)
    t44origen = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't44diagnosticosoptometria'


class T45Tomamuestras(models.Model):
    t45id = models.AutoField(primary_key=True)
    t45orden = models.IntegerField(blank=True, null=True)
    t45fecha = models.DateTimeField(blank=True, null=True)
    t45usuario = models.IntegerField(blank=True, null=True)
    t45ayunas = models.NullBooleanField()
    t45fumo = models.NullBooleanField()
    t45horafumo = models.CharField(max_length=50, blank=True, null=True)
    t45droga = models.NullBooleanField()
    t45cualdroga = models.CharField(max_length=50, blank=True, null=True)
    t45medicamento = models.NullBooleanField()
    t45quemedicamento = models.CharField(max_length=50, blank=True, null=True)
    t45fum = models.DateField(blank=True, null=True)
    t45enfermedad = models.NullBooleanField()
    t45huevo = models.NullBooleanField()
    t45ejercicio = models.NullBooleanField()
    t45tos = models.NullBooleanField()
    t45tiempotos = models.CharField(max_length=50, blank=True, null=True)
    t45mediotransporte = models.CharField(max_length=200, blank=True, null=True)
    t45observaciones = models.TextField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    t45estado = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't45tomamuestras'


class T46Exameneslaboratorio(models.Model):
    t46id = models.AutoField(primary_key=True)
    t46examen = models.IntegerField(blank=True, null=True)
    t46nombre = models.CharField(max_length=250, blank=True, null=True)
    t46tipo = models.IntegerField(blank=True, null=True)
    t46referencia = models.CharField(max_length=200, blank=True, null=True)
    t46unidad = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t46valores = models.TextField(blank=True, null=True)  # This field type is a guess.
    t46valores2 = models.TextField(blank=True, null=True)  # This field type is a guess.
    t46titulo = models.CharField(max_length=50, blank=True, null=True)
    t46titulo2 = models.CharField(max_length=50, blank=True, null=True)
    t46orden = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't46exameneslaboratorio'


class T47Laboratorios(models.Model):
    t47id = models.AutoField(primary_key=True)
    t47orden = models.IntegerField(blank=True, null=True)
    t47fecha = models.DateTimeField(blank=True, null=True)
    t47usuario = models.IntegerField(blank=True, null=True)
    t47examenlaboratorio = models.IntegerField(blank=True, null=True)
    t47valor = models.CharField(max_length=250, blank=True, null=True)
    t47valor2 = models.CharField(max_length=250, blank=True, null=True)
    t47observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t47estado = models.IntegerField(blank=True, null=True)
    t47observaciones = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't47laboratorios'


class T48Visiometrias(models.Model):
    t48id = models.AutoField(primary_key=True)
    t48orden = models.IntegerField(blank=True, null=True)
    t48usuario = models.IntegerField(blank=True, null=True)
    t48fecha = models.DateTimeField(blank=True, null=True)
    t48antecedentes = models.TextField(blank=True, null=True)  # This field type is a guess.
    t48antecedentesoculares = models.TextField(blank=True, null=True)  # This field type is a guess.
    t48signos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t48otrosantecedentes = models.TextField(blank=True, null=True)
    t48refraccion = models.CharField(max_length=50, blank=True, null=True)
    t48corregido = models.NullBooleanField()
    t48cualrefraccion = models.TextField(blank=True, null=True)
    t48tiempoevolucion = models.FloatField(blank=True, null=True)
    t48correccion = models.CharField(max_length=100, blank=True, null=True)
    t48tiempoultimoexamen = models.FloatField(blank=True, null=True)
    t48frecuenciauso = models.CharField(max_length=100, blank=True, null=True)
    t48tiempoultimaformula = models.FloatField(blank=True, null=True)
    t48observacionsignos = models.TextField(blank=True, null=True)
    t48odvls = models.CharField(max_length=50, blank=True, null=True)
    t48odvps = models.CharField(max_length=50, blank=True, null=True)
    t48odvlc = models.CharField(max_length=50, blank=True, null=True)
    t48odvpc = models.CharField(max_length=50, blank=True, null=True)
    t48odvl = models.CharField(max_length=50, blank=True, null=True)
    t48odvp = models.CharField(max_length=50, blank=True, null=True)
    t48pinguecula = models.NullBooleanField()
    t48resequedad = models.NullBooleanField()
    t48inyeccion = models.NullBooleanField()
    t48hiperemia = models.NullBooleanField()
    t48ptosis = models.NullBooleanField()
    t48pterigio = models.NullBooleanField()
    t48secrecion = models.NullBooleanField()
    t48edema = models.NullBooleanField()
    t48blefaritis = models.NullBooleanField()
    t48covervisionlejana = models.CharField(max_length=50, blank=True, null=True)
    t48covervisionproxima = models.CharField(max_length=50, blank=True, null=True)
    t48puntoconvergencia = models.CharField(max_length=50, blank=True, null=True)
    t48estereopsisod = models.CharField(max_length=50, blank=True, null=True)
    t48estereopsisoi = models.CharField(max_length=50, blank=True, null=True)
    t48cromaticaod = models.CharField(max_length=50, blank=True, null=True)
    t48cromaticaoi = models.CharField(max_length=50, blank=True, null=True)
    t48campovisualod = models.CharField(max_length=50, blank=True, null=True)
    t48campovisualoi = models.CharField(max_length=50, blank=True, null=True)
    t48oftalmoscopiaod = models.CharField(max_length=50, blank=True, null=True)
    t48oftalmoscopiaoi = models.CharField(max_length=50, blank=True, null=True)
    t48emetropia = models.CharField(max_length=50, blank=True, null=True)
    t48miopia = models.CharField(max_length=50, blank=True, null=True)
    t48hipermetropia = models.CharField(max_length=50, blank=True, null=True)
    t48astigmatismo = models.CharField(max_length=50, blank=True, null=True)
    t48presbicia = models.CharField(max_length=50, blank=True, null=True)
    t48ambliopia = models.CharField(max_length=50, blank=True, null=True)
    t48motilidad = models.CharField(max_length=50, blank=True, null=True)
    t48convergencia = models.CharField(max_length=50, blank=True, null=True)
    t48refraccionnocorregido = models.CharField(max_length=50, blank=True, null=True)
    t48refraccionadecuadamente = models.CharField(max_length=50, blank=True, null=True)
    t48refraccioninadecuadamente = models.CharField(max_length=50, blank=True, null=True)
    t48norequiererefraccion = models.CharField(max_length=50, blank=True, null=True)
    t48recomendaciones = models.TextField(blank=True, null=True)  # This field type is a guess.
    t48observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t48observacionriesgos = models.TextField(blank=True, null=True)
    t48oivls = models.CharField(max_length=50, blank=True, null=True)
    t48oivps = models.CharField(max_length=50, blank=True, null=True)
    t48oivlc = models.CharField(max_length=50, blank=True, null=True)
    t48oivpc = models.CharField(max_length=50, blank=True, null=True)
    t48oivl = models.CharField(max_length=50, blank=True, null=True)
    t48oivp = models.CharField(max_length=50, blank=True, null=True)
    t48retinoscopiaod = models.CharField(max_length=50, blank=True, null=True)
    t48retinoscopiaoi = models.CharField(max_length=50, blank=True, null=True)
    t48resultado = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't48visiometrias'


class T49Riesgosvisiometria(models.Model):
    t49id = models.AutoField(primary_key=True)
    t49visiometria = models.IntegerField(blank=True, null=True)
    t49tipo = models.IntegerField(blank=True, null=True)
    t49valor = models.NullBooleanField()
    t49tiempoanios = models.FloatField(blank=True, null=True)
    t49tiempohoras = models.FloatField(blank=True, null=True)
    t49proteccion = models.NullBooleanField()
    t49tipoproteccion = models.CharField(max_length=250, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't49riesgosvisiometria'


class T50Diagnosticosvisiometria(models.Model):
    t50id = models.AutoField(primary_key=True)
    t50examen = models.IntegerField(blank=True, null=True)
    t50cie10 = models.IntegerField(blank=True, null=True)
    t50tipo = models.CharField(max_length=100, blank=True, null=True)
    t50origen = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't50diagnosticosvisiometria'


class T51Limitaciones(models.Model):
    t51id = models.AutoField(primary_key=True)
    t51examen = models.IntegerField(blank=True, null=True)
    t51cuales = models.TextField(blank=True, null=True)
    t51condiciones = models.TextField(blank=True, null=True)
    t51agentes = models.TextField(blank=True, null=True)
    t51tipo = models.CharField(max_length=50, blank=True, null=True)
    t51recomendaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't51limitaciones'


class T52Espirometrias(models.Model):
    t52id = models.AutoField(primary_key=True)
    t52orden = models.IntegerField(blank=True, null=True)
    t52fecha = models.DateTimeField(blank=True, null=True)
    t52usuario = models.IntegerField(blank=True, null=True)
    t52polvo = models.NullBooleanField()
    t52humos = models.NullBooleanField()
    t52vapores = models.NullBooleanField()
    t52neblinas = models.NullBooleanField()
    t52tiempoexposicion = models.FloatField(blank=True, null=True)
    t52epp = models.NullBooleanField()
    t52cualesepp = models.TextField(blank=True, null=True)
    t52fuma = models.NullBooleanField()
    t52tiempofuma = models.FloatField(blank=True, null=True)
    t52exfumador = models.NullBooleanField()
    t52fechaexfumador = models.FloatField(blank=True, null=True)
    t52deporte = models.NullBooleanField()
    t52cualdeporte = models.TextField(blank=True, null=True)
    t52frecuenciadeporte = models.TextField(blank=True, null=True)
    t52dificultad = models.NullBooleanField()
    t52esfuerzo = models.NullBooleanField()
    t52dolor = models.NullBooleanField()
    t52dondedolor = models.TextField(blank=True, null=True)
    t52tos = models.NullBooleanField()
    t52esputo = models.NullBooleanField()
    t52alergia = models.NullBooleanField()
    t52cualalergia = models.TextField(blank=True, null=True)
    t52enfermedad = models.NullBooleanField()
    t52cualenfermedad = models.TextField(blank=True, null=True)
    t52asma = models.NullBooleanField()
    t52cirugia = models.NullBooleanField()
    t52cualcirugia = models.TextField(blank=True, null=True)
    t52otraenfermedad = models.NullBooleanField()
    t52cualotraenfermedad = models.TextField(blank=True, null=True)
    t52espirometrias = models.NullBooleanField()
    t52resultadoanterior = models.TextField(blank=True, null=True)
    t52patron = models.CharField(max_length=50, blank=True, null=True)
    t52grado = models.CharField(max_length=50, blank=True, null=True)
    t52aceptable = models.NullBooleanField()
    t52marca = models.TextField(blank=True, null=True)
    t52escala = models.CharField(max_length=50, blank=True, null=True)
    t52calibracion = models.NullBooleanField()
    t52recomendaciones = models.TextField(blank=True, null=True)  # This field type is a guess.
    t52observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t52gases = models.NullBooleanField()
    t52resultado = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't52espirometrias'


class T53Diagnosticosespirometria(models.Model):
    t53id = models.AutoField(primary_key=True)
    t53examen = models.IntegerField(blank=True, null=True)
    t53cie10 = models.IntegerField(blank=True, null=True)
    t53tipo = models.CharField(max_length=100, blank=True, null=True)
    t53origen = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't53diagnosticosespirometria'


class T54Activosfijos(models.Model):
    t54id = models.AutoField(primary_key=True)
    t54sede = models.IntegerField(blank=True, null=True)
    t54codigo = models.CharField(max_length=100, blank=True, null=True)
    t54nombre = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't54activosfijos'


class T55Chequeosconsultorio(models.Model):
    t55id = models.AutoField(primary_key=True)
    t55fecha = models.DateTimeField(blank=True, null=True)
    t55dia = models.DateField(blank=True, null=True)
    t55usuario = models.IntegerField(blank=True, null=True)
    t55consultorio = models.IntegerField(blank=True, null=True)
    t55chequeo = models.TextField(blank=True, null=True)  # This field type is a guess.
    t55observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't55chequeosconsultorio'


class T56Remisiones(models.Model):
    t56id = models.AutoField(primary_key=True)
    t56orden = models.IntegerField(blank=True, null=True)
    t56proveedor = models.IntegerField(blank=True, null=True)
    t56fecha = models.DateTimeField(blank=True, null=True)
    t56observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t56usuario = models.IntegerField(blank=True, null=True)
    t56examen = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't56remisiones'


class T57Comprobantesventa(models.Model):
    t57id = models.AutoField(primary_key=True)
    t57fecha = models.DateField(blank=True, null=True)
    t57usuario = models.IntegerField(blank=True, null=True)
    t57estado = models.IntegerField(blank=True, null=True)
    t57tipo = models.IntegerField(blank=True, null=True)
    t57cliente = models.IntegerField(blank=True, null=True)
    t57paciente = models.IntegerField(blank=True, null=True)
    t57orden = models.IntegerField(blank=True, null=True)
    t57plazo = models.IntegerField(blank=True, null=True)
    t57observaciones = models.TextField(blank=True, null=True)
    t57prefijo = models.CharField(max_length=50, blank=True, null=True)
    t57consecutivo = models.IntegerField(blank=True, null=True)
    t57sede = models.IntegerField(blank=True, null=True)
    t57resolucion = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t57total = models.FloatField(blank=True, null=True)
    t57comprobante = models.IntegerField(blank=True, null=True)
    t57anulado = models.NullBooleanField()
    t57facturar = models.CharField(max_length=100, blank=True, null=True)
    t57idanterior = models.IntegerField(blank=True, null=True)
    t57centrodecosto = models.IntegerField(blank=True, null=True)
    t57adjuntos = models.NullBooleanField()
    t57observacionesanulado = models.TextField(blank=True, null=True)
    t57comprobantesanulado = models.TextField(blank=True, null=True)  # This field type is a guess.
    t57tipoenvio = models.CharField(max_length=100, blank=True, null=True)
    t57relacion = models.NullBooleanField()
    t57cumplidoprogramacion = models.NullBooleanField()
    t57entidad = models.IntegerField(blank=True, null=True)
    t57radicada = models.IntegerField(blank=True, null=True)
    t57observacionradicada = models.TextField(blank=True, null=True)
    t57modificable = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't57comprobantesventa'


class T58Pagoscomprobante(models.Model):
    t58id = models.AutoField(primary_key=True)
    t58comprobante = models.IntegerField(blank=True, null=True)
    t58formapago = models.IntegerField(blank=True, null=True)
    t58valor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t58fecha = models.DateField(blank=True, null=True)
    t58cuenta = models.IntegerField(blank=True, null=True)
    t58comprobanteingreso = models.IntegerField(blank=True, null=True)
    t58anulado = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't58pagoscomprobante'


class T59Detallecomprobanteventa(models.Model):
    t59id = models.AutoField(primary_key=True)
    t59comprobante = models.IntegerField(blank=True, null=True)
    t59servicio = models.IntegerField(blank=True, null=True)
    t59iva = models.FloatField(blank=True, null=True)
    t59consumo = models.FloatField(blank=True, null=True)
    t59valor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t59cantidad = models.FloatField(blank=True, null=True)
    t59total = models.FloatField(blank=True, null=True)
    t59anulado = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't59detallecomprobanteventa'


class T60Resoluciones(models.Model):
    t60id = models.AutoField(primary_key=True)
    t60sede = models.IntegerField(blank=True, null=True)
    t60resolucion = models.TextField(blank=True, null=True)
    t60fecha = models.DateField(blank=True, null=True)
    t60desde = models.DateField(blank=True, null=True)
    t60hasta = models.DateField(blank=True, null=True)
    t60prefijo = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't60resoluciones'


class T61Formasdepago(models.Model):
    t61id = models.AutoField(primary_key=True)
    t61sede = models.IntegerField(blank=True, null=True)
    t61nombre = models.CharField(max_length=30 ,blank=True, null=True)
    t61efectivo = models.NullBooleanField()
    t61credito = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t61cod = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't61formasdepago'


class T62Anexoosteomuscular(models.Model):
    t62id = models.AutoField(primary_key=True)
    t62orden = models.IntegerField(blank=True, null=True)
    t62fecha = models.DateTimeField(blank=True, null=True)
    t62usuario = models.IntegerField(blank=True, null=True)
    t62exposicion = models.NullBooleanField()
    t62cargas = models.NullBooleanField()
    t62horascargas = models.FloatField(blank=True, null=True)
    t62pesocargas = models.CharField(max_length=50, blank=True, null=True)
    t62halar = models.CharField(max_length=30 ,blank=True, null=True)
    t62empujar = models.CharField(max_length=30 ,blank=True, null=True)
    t62cargar = models.CharField(max_length=30 ,blank=True, null=True)
    t62descargar = models.CharField(max_length=30 ,blank=True, null=True)
    t62levantar = models.CharField(max_length=30 ,blank=True, null=True)
    t62actividad = models.CharField(max_length=50, blank=True, null=True)
    t62objetos = models.CharField(max_length=50, blank=True, null=True)
    t62trayecto = models.CharField(max_length=50, blank=True, null=True)
    t62videoterminales = models.CharField(max_length=50, blank=True, null=True)
    t62sedente = models.CharField(max_length=30 ,blank=True, null=True)
    t62bipeda = models.CharField(max_length=30 ,blank=True, null=True)
    t62cunclillas = models.CharField(max_length=30 ,blank=True, null=True)
    t62rodillas = models.CharField(max_length=30 ,blank=True, null=True)
    t62jornadas = models.CharField(max_length=50, blank=True, null=True)
    t62herramientas = models.CharField(max_length=30 ,blank=True, null=True)
    t62horasherramientas = models.FloatField(blank=True, null=True)
    t62vibra = models.CharField(max_length=30 ,blank=True, null=True)
    t62horasvibra = models.FloatField(blank=True, null=True)
    t62movimientoshombro = models.CharField(max_length=30 ,blank=True, null=True)
    t62movimientoscodo = models.CharField(max_length=30 ,blank=True, null=True)
    t62movimientosmuneca = models.CharField(max_length=30 ,blank=True, null=True)
    t62observaciones = models.TextField(blank=True, null=True)
    t62lateral = models.CharField(max_length=50, blank=True, null=True)
    t62posterior = models.CharField(max_length=50, blank=True, null=True)
    t62palpacion = models.TextField(blank=True, null=True)
    t62masas = models.TextField(blank=True, null=True)
    t62marcha = models.TextField(blank=True, null=True)
    t62flexion = models.TextField(blank=True, null=True)
    t62rotacion = models.TextField(blank=True, null=True)
    t62observacionalcance = models.TextField(blank=True, null=True)
    t62sve = models.TextField(blank=True, null=True)
    t62concepto = models.CharField(max_length=50, blank=True, null=True)
    t62observacionesanexo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t62empresas = models.TextField(blank=True, null=True)  # This field type is a guess.
    t62revision = models.TextField(blank=True, null=True)  # This field type is a guess.
    t62alcance = models.TextField(blank=True, null=True)  # This field type is a guess.
    t62otrossegmentos = models.TextField(blank=True, null=True)  # This field type is a guess.
    t62observacionreflejos = models.TextField(blank=True, null=True)
    t62observacionpruebas = models.TextField(blank=True, null=True)
    t62pruebas = models.TextField(blank=True, null=True)  # This field type is a guess.
    t62reflejos = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 't62anexoosteomuscular'


class T63Anexoalturas(models.Model):
    t63id = models.AutoField(primary_key=True)
    t63orden = models.IntegerField(blank=True, null=True)
    t63fecha = models.DateTimeField(blank=True, null=True)
    t63usuario = models.IntegerField(blank=True, null=True)
    t63antecedentes = models.TextField(blank=True, null=True)  # This field type is a guess.
    t63otros = models.TextField(blank=True, null=True)
    t63observacionantecedentes = models.TextField(blank=True, null=True)
    t63sensaciones = models.TextField(blank=True, null=True)  # This field type is a guess.
    t63otrassensaciones = models.TextField(blank=True, null=True)
    t63examenes = models.TextField(blank=True, null=True)  # This field type is a guess.
    t63resultado = models.CharField(max_length=50, blank=True, null=True)
    t63observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t63otrosexamenes = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 't63anexoalturas'


class T64Anexocardiovascular(models.Model):
    t64id = models.AutoField(primary_key=True)
    t64orden = models.IntegerField(blank=True, null=True)
    t64fecha = models.DateTimeField(blank=True, null=True)
    t64usuario = models.IntegerField(blank=True, null=True)
    t64edad = models.CharField(max_length=50, blank=True, null=True)
    t64historial = models.CharField(max_length=50, blank=True, null=True)
    t64estres = models.CharField(max_length=50, blank=True, null=True)
    t64ejercicio = models.CharField(max_length=50, blank=True, null=True)
    t64cigarrillo = models.CharField(max_length=50, blank=True, null=True)
    t64peso = models.CharField(max_length=50, blank=True, null=True)
    t64colesterol = models.CharField(max_length=50, blank=True, null=True)
    t64colesterolhdl = models.CharField(max_length=50, blank=True, null=True)
    t64diabetesclasificacion = models.CharField(max_length=50, blank=True, null=True)
    t64diabetesantecedentes = models.CharField(max_length=50, blank=True, null=True)
    t64presion = models.CharField(max_length=50, blank=True, null=True)
    t64subtotala = models.CharField(max_length=50, blank=True, null=True)
    t64subtotalb = models.CharField(max_length=50, blank=True, null=True)
    t64subtotalc = models.CharField(max_length=50, blank=True, null=True)
    t64puntuacion = models.CharField(max_length=50, blank=True, null=True)
    t64riesgo = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't64anexocardiovascular'


class T65Otrosexamenes(models.Model):
    t65id = models.AutoField(primary_key=True)
    t65orden = models.IntegerField(blank=True, null=True)
    t65fecha = models.DateField(blank=True, null=True)
    t65usuario = models.IntegerField(blank=True, null=True)
    t65tipo = models.CharField(max_length=100, blank=True, null=True)
    t65examen = models.IntegerField(blank=True, null=True)
    t65observacion = models.TextField(blank=True, null=True)
    t65estado = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t65numerocontrol = models.FloatField(blank=True, null=True)
    t65fechafinalizado = models.DateField(blank=True, null=True)
    t65concepto = models.TextField(blank=True, null=True)
    t65tension = models.TextField(blank=True, null=True)
    t65frecuencia = models.TextField(blank=True, null=True)
    t65talla = models.TextField(blank=True, null=True)
    t65peso = models.TextField(blank=True, null=True)
    t65pregunta1 = models.TextField(blank=True, null=True)
    t65pregunta2 = models.TextField(blank=True, null=True)
    t65pregunta3 = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't65otrosexamenes'


class T66Crcaudicion(models.Model):
    t66id = models.AutoField(primary_key=True)
    t66orden = models.IntegerField(blank=True, null=True)
    t66fecha = models.DateField(blank=True, null=True)
    t66usuario = models.IntegerField(blank=True, null=True)
    t66examenes = models.NullBooleanField()
    t66expuesto = models.NullBooleanField()
    t66operado = models.NullBooleanField()
    t66golpes = models.NullBooleanField()
    t66mareo = models.NullBooleanField()
    t66musica = models.NullBooleanField()
    t66dificultad = models.NullBooleanField()
    t66supuracion = models.NullBooleanField()
    t66disminuido = models.NullBooleanField()
    t66ayuda = models.NullBooleanField()
    t66otoscopiaderecho = models.CharField(max_length=30 ,blank=True, null=True)
    t66otoscopiaizquierdo = models.CharField(max_length=30 ,blank=True, null=True)
    t66ptaderecho = models.CharField(max_length=30 ,blank=True, null=True)
    t66ptaizquierdo = models.CharField(max_length=30 ,blank=True, null=True)
    t66estado = models.IntegerField(blank=True, null=True)
    t66resultado = models.CharField(max_length=30 ,blank=True, null=True)
    t66restricciones = models.TextField(blank=True, null=True)
    t66oda250 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oda500 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oda1000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oda2000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oda3000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oda4000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oda6000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oda8000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oia250 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oia500 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oia1000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oia2000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oia3000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oia4000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oia6000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66oia8000 = models.CharField(max_length=30 ,blank=True, null=True)
    t66observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't66crcaudicion'


class T67Crcocupacional(models.Model):
    t67id = models.AutoField(primary_key=True)
    t67orden = models.IntegerField(blank=True, null=True)
    t67fecha = models.DateField(blank=True, null=True)
    t67usuario = models.IntegerField(blank=True, null=True)
    t67peso = models.FloatField(blank=True, null=True)
    t67altura = models.FloatField(blank=True, null=True)
    t67frecuenciarespiratoria = models.NullBooleanField()
    t67frecuenciacardiaca = models.NullBooleanField()
    t67tensiondiastolica = models.NullBooleanField()
    t67tensionsistolica = models.NullBooleanField()
    t67amputaciones = models.NullBooleanField()
    t67edemas = models.NullBooleanField()
    t67dificultad = models.NullBooleanField()
    t67dificultadrespiratoria = models.NullBooleanField()
    t67dificultaddormir = models.NullBooleanField()
    t67hipertension = models.NullBooleanField()
    t67diabetes = models.NullBooleanField()
    t67coagulacion = models.CharField(max_length=30 ,blank=True, null=True)
    t67marcapasos = models.NullBooleanField()
    t67infarto = models.NullBooleanField()
    t67dialisis = models.NullBooleanField()
    t67renal = models.NullBooleanField()
    t67disnea = models.NullBooleanField()
    t67hipoglicemia = models.NullBooleanField()
    t67epilepsia = models.NullBooleanField()
    t67hipotiroidismo = models.NullBooleanField()
    t67espasmos = models.NullBooleanField()
    t67accidentes = models.NullBooleanField()
    t67anemia = models.NullBooleanField()
    t67quimioterapia = models.NullBooleanField()
    t67alucinogenos = models.NullBooleanField()
    t67alcohol = models.NullBooleanField()
    t67horario = models.NullBooleanField()
    t67estado = models.IntegerField(blank=True, null=True)
    t67resultado = models.TextField(blank=True, null=True)
    t67restricciones = models.TextField(blank=True, null=True)
    t67observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't67crcocupacional'


class T68Crcvision(models.Model):
    t68id = models.AutoField(primary_key=True)
    t68orden = models.IntegerField(blank=True, null=True)
    t68fecha = models.DateField(blank=True, null=True)
    t68usuario = models.IntegerField(blank=True, null=True)
    t68ojoderecho = models.NullBooleanField()
    t68ojoizquierdo = models.NullBooleanField()
    t68gafas = models.NullBooleanField()
    t68cirugia = models.NullBooleanField()
    t68nistagmus = models.NullBooleanField()
    t68desviacion = models.NullBooleanField()
    t68ptosis = models.NullBooleanField()
    t68desprendimiento = models.NullBooleanField()
    t68doble = models.NullBooleanField()
    t68golpe = models.NullBooleanField()
    t68procedimiento = models.NullBooleanField()
    t68avld = models.CharField(max_length=30 ,blank=True, null=True)
    t68avli = models.CharField(max_length=30 ,blank=True, null=True)
    t68avcd = models.CharField(max_length=30 ,blank=True, null=True)
    t68avci = models.CharField(max_length=30 ,blank=True, null=True)
    t68nocturna = models.NullBooleanField()
    t68horizontal = models.NullBooleanField()
    t68vertical = models.NullBooleanField()
    t68profundidad = models.CharField(max_length=30 ,blank=True, null=True)
    t68colores = models.NullBooleanField()
    t68encandilamiento = models.CharField(max_length=30 ,blank=True, null=True)
    t68aprobo = models.NullBooleanField()
    t68campovisualderecho = models.CharField(max_length=30 ,blank=True, null=True)
    t68campovisualizquierdo = models.CharField(max_length=30 ,blank=True, null=True)
    t68inferior = models.NullBooleanField()
    t68superior = models.NullBooleanField()
    t68estado = models.IntegerField(blank=True, null=True)
    t68resultado = models.TextField(blank=True, null=True)
    t68restricciones = models.TextField(blank=True, null=True)
    t68observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't68crcvision'


class T69Crcmotricidad(models.Model):
    t69id = models.AutoField(primary_key=True)
    t69orden = models.IntegerField(blank=True, null=True)
    t69fecha = models.DateField(blank=True, null=True)
    t69usuario = models.IntegerField(blank=True, null=True)
    t69atencionacierto = models.CharField(max_length=30 ,blank=True, null=True)
    t69atencionrespuesta = models.CharField(max_length=30 ,blank=True, null=True)
    t69atencionerrores = models.CharField(max_length=30 ,blank=True, null=True)
    t69reaccionesrespuesta = models.CharField(max_length=30 ,blank=True, null=True)
    t69reaccionesacierto = models.CharField(max_length=30 ,blank=True, null=True)
    t69reaccioneserrores = models.CharField(max_length=30 ,blank=True, null=True)
    t69reaccion = models.CharField(max_length=30 ,blank=True, null=True)
    t69coordinacionerror = models.CharField(max_length=30 ,blank=True, null=True)
    t69coordinacionerrores = models.CharField(max_length=30 ,blank=True, null=True)
    t69percepcion = models.CharField(max_length=30 ,blank=True, null=True)
    t69estado = models.IntegerField(blank=True, null=True)
    t69resultado = models.TextField(blank=True, null=True)
    t69restricciones = models.TextField(blank=True, null=True)
    t69observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't69crcmotricidad'


class T70Crcpsicologia(models.Model):
    t70id = models.AutoField(primary_key=True)
    t70orden = models.IntegerField(blank=True, null=True)
    t70fecha = models.DateField(blank=True, null=True)
    t70usuario = models.IntegerField(blank=True, null=True)
    t70personalidad = models.CharField(max_length=30 ,blank=True, null=True)
    t70sustancias = models.CharField(max_length=30 ,blank=True, null=True)
    t70inteligencia = models.CharField(max_length=30 ,blank=True, null=True)
    t70estado = models.IntegerField(blank=True, null=True)
    t70resultado = models.TextField(blank=True, null=True)
    t70restricciones = models.TextField(blank=True, null=True)
    t70observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't70crcpsicologia'


class T71Prospectos(models.Model):
    t71id = models.AutoField(primary_key=True)
    t71nit = models.CharField(max_length=30 ,blank=True, null=True)
    t71razonsocial = models.TextField(blank=True, null=True)
    t71fecha = models.DateField(blank=True, null=True)
    t71usuario = models.IntegerField(blank=True, null=True)
    t71tipo = models.CharField(max_length=30 ,blank=True, null=True)
    t71nombrecomercial = models.TextField(blank=True, null=True)
    t71direccion = models.TextField(blank=True, null=True)
    t71departamento = models.IntegerField(blank=True, null=True)
    t71ciudad = models.IntegerField(blank=True, null=True)
    t71telefono = models.TextField(blank=True, null=True)
    t71celular = models.TextField(blank=True, null=True)
    t71fax = models.TextField(blank=True, null=True)
    t71email = models.TextField(blank=True, null=True)
    t71sector = models.TextField(blank=True, null=True)
    t71numerotrabajadores = models.TextField(blank=True, null=True)
    t71emailrues = models.TextField(blank=True, null=True)
    t71emailelinforma = models.TextField(blank=True, null=True)
    t71contacto = models.TextField(blank=True, null=True)
    t71cargo = models.TextField(blank=True, null=True)
    t71emailcontacto = models.TextField(blank=True, null=True)
    t71telefonocontacto = models.TextField(blank=True, null=True)
    t71fechasolicitud = models.DateField(blank=True, null=True)
    t71horasolicitud = models.TimeField(blank=True, null=True)
    t71medio = models.TextField(blank=True, null=True)
    t71estado = models.IntegerField(blank=True, null=True)
    t71enviada = models.NullBooleanField()
    t71radicada = models.NullBooleanField()
    t71observacion = models.TextField(blank=True, null=True)
    t71primernombre = models.TextField(blank=True, null=True)
    t71segundonombre = models.TextField(blank=True, null=True)
    t71primerapellido = models.TextField(blank=True, null=True)
    t71segundoapellido = models.TextField(blank=True, null=True)
    t71extension = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t71idanterior = models.IntegerField(blank=True, null=True)
    t71empresa = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't71prospectos'


class T72Cotizaciones(models.Model):
    t72id = models.AutoField(primary_key=True)
    t72prospecto = models.IntegerField(blank=True, null=True)
    t72usuario = models.IntegerField(blank=True, null=True)
    t72ciudad = models.IntegerField(blank=True, null=True)
    t72examenes = models.TextField(blank=True, null=True)  # This field type is a guess.
    t72observacion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't72cotizaciones'


class T73Seguimientos(models.Model):
    t73id = models.AutoField(primary_key=True)
    t73prospecto = models.IntegerField(blank=True, null=True)
    t73usuario = models.IntegerField(blank=True, null=True)
    t73fecha = models.DateField(blank=True, null=True)
    t73hora = models.TimeField(blank=True, null=True)
    t73tipo = models.CharField(max_length=30 ,blank=True, null=True)
    t73estadocorreo = models.CharField(max_length=30 ,blank=True, null=True)
    t73contacto = models.TextField(blank=True, null=True)
    t73cargo = models.TextField(blank=True, null=True)
    t73correo = models.TextField(blank=True, null=True)
    t73telefono = models.TextField(blank=True, null=True)
    t73descripcion = models.TextField(blank=True, null=True)
    t73detallecorreo = models.TextField(blank=True, null=True)
    t73proximo = models.DateField(blank=True, null=True)
    t73tipoproximo = models.CharField(max_length=30 ,blank=True, null=True)
    t73encargado = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t73idanterior = models.IntegerField(blank=True, null=True)
    t73horaproxima = models.TimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't73seguimientos'


class T74Eventos(models.Model):
    t74id = models.AutoField(primary_key=True)
    t74usuario = models.IntegerField(blank=True, null=True)
    t74hora = models.TimeField(blank=True, null=True)
    t74fecha = models.DateField(blank=True, null=True)
    t74nombre = models.CharField(max_length=30 ,blank=True, null=True)
    t74descripcion = models.TextField(blank=True, null=True)
    t74html = models.TextField(blank=True, null=True)
    t74ciudad = models.IntegerField(blank=True, null=True)
    t74direccion = models.TextField(blank=True, null=True)
    t74capacidad = models.IntegerField(blank=True, null=True)
    t74youtube = models.TextField(blank=True, null=True)
    t74duracion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't74eventos'


class T75Invitados(models.Model):
    t75id = models.AutoField(primary_key=True)
    t75evento = models.IntegerField(blank=True, null=True)
    t75nit = models.CharField(max_length=30 ,blank=True, null=True)
    t75razonsocial = models.TextField(blank=True, null=True)
    t75telefono = models.CharField(max_length=30 ,blank=True, null=True)
    t75email = models.TextField(blank=True, null=True)
    t75confirmado = models.NullBooleanField()
    t75cantidad = models.IntegerField(blank=True, null=True)
    t75usuario = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t75direccion = models.TextField(blank=True, null=True)
    t75barrio = models.TextField(blank=True, null=True)
    t75localidad = models.TextField(blank=True, null=True)
    t75sector = models.CharField(max_length=250, blank=True, null=True)
    t75trabajadores = models.IntegerField(blank=True, null=True)
    t75hseq = models.TextField(blank=True, null=True)
    t75correohseq = models.TextField(blank=True, null=True)
    t75telefonohseq = models.TextField(blank=True, null=True)
    t75confirmados = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 't75invitados'


class T76Empresasprospecto(models.Model):
    t76id = models.AutoField(primary_key=True)
    t76nit = models.CharField(max_length=30 ,blank=True, null=True)
    t76razonsocial = models.CharField(max_length=30 ,blank=True, null=True)
    t76ips = models.CharField(max_length=30 ,blank=True, null=True)
    t76valor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't76empresasprospecto'


class T77Seguimientosevento(models.Model):
    t77id = models.AutoField(primary_key=True)
    t77evento = models.IntegerField(blank=True, null=True)
    t77usuario = models.IntegerField(blank=True, null=True)
    t77fecha = models.DateField(blank=True, null=True)
    t77contacto = models.CharField(max_length=30 ,blank=True, null=True)
    t77cargo = models.CharField(max_length=30 ,blank=True, null=True)
    t77telefono = models.CharField(max_length=30 ,blank=True, null=True)
    t77email = models.TextField(blank=True, null=True)
    t77descripcion = models.TextField(blank=True, null=True)
    t77invitado = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t77proxima = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't77seguimientosevento'


class T78Seguimientosorden(models.Model):
    t78id = models.AutoField(primary_key=True)
    t78orden = models.IntegerField(blank=True, null=True)
    t78usuario = models.IntegerField(blank=True, null=True)
    t78fecha = models.DateField(blank=True, null=True)
    t78hora = models.TimeField(blank=True, null=True)
    t78detalle = models.TextField(blank=True, null=True)
    t78telefono = models.TextField(blank=True, null=True)
    t78contacto = models.TextField(blank=True, null=True)
    t78cargo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t78llego = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't78seguimientosorden'


class T79Preordenes(models.Model):
    t79id = models.AutoField(primary_key=True)
    t79consecutivo = models.IntegerField(blank=True, null=True)
    t79cliente = models.IntegerField(blank=True, null=True)
    t79entidad = models.IntegerField(blank=True, null=True)
    t79fecha = models.DateField(blank=True, null=True)
    t79tipo = models.IntegerField(blank=True, null=True)
    t79rednacional = models.NullBooleanField()
    t79paciente = models.IntegerField(blank=True, null=True)
    t79telefono = models.CharField(max_length=50, blank=True, null=True)
    t79email = models.CharField(max_length=200, blank=True, null=True)
    t79ciudadexamen = models.IntegerField(blank=True, null=True)
    t79cargo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t79examenes = models.TextField(blank=True, null=True)
    t79usuario = models.IntegerField(blank=True, null=True)
    t79ips = models.IntegerField(blank=True, null=True)
    t79utilizada = models.NullBooleanField()
    t79centrodecosto = models.IntegerField(blank=True, null=True)
    t79unidadgestion = models.IntegerField(blank=True, null=True)
    t79cita = models.NullBooleanField()
    t79hora = models.TextField(blank=True, null=True)
    t79area = models.TextField(blank=True, null=True)
    t79restricciones = models.TextField(blank=True, null=True)  # This field type is a guess.
    t79observaciones = models.TextField(blank=True, null=True)
    t79metodocontacto = models.TextField(blank=True, null=True)
    t79armas = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't79preordenes'


class T80Ipsaliadas(models.Model):
    t80id = models.AutoField(primary_key=True)
    t80ciudad = models.IntegerField(blank=True, null=True)
    t80activa = models.NullBooleanField()
    t80nit = models.CharField(max_length=100, blank=True, null=True)
    t80razonsocial = models.TextField(blank=True, null=True)
    t80direccion = models.TextField(blank=True, null=True)
    t80telefono = models.TextField(blank=True, null=True)
    t80email = models.TextField(blank=True, null=True)
    t80contacto = models.TextField(blank=True, null=True)
    t80cargo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t80telefonocontacto = models.CharField(max_length=250, blank=True, null=True)
    t80emailcontacto = models.CharField(max_length=250, blank=True, null=True)
    t80telefono2 = models.CharField(max_length=250, blank=True, null=True)
    t80horario = models.TextField(blank=True, null=True)
    t80horariosabado = models.TextField(blank=True, null=True)
    t80sede = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't80ipsaliadas'


class T81Crccertificacion(models.Model):
    t81id = models.AutoField(primary_key=True)
    t81orden = models.IntegerField(blank=True, null=True)
    t81fecha = models.DateField(blank=True, null=True)
    t81usuario = models.IntegerField(blank=True, null=True)
    t81estado = models.IntegerField(blank=True, null=True)
    t81resultado = models.CharField(max_length=100, blank=True, null=True)
    t81restricciones = models.CharField(max_length=100, blank=True, null=True)
    t81observaciones = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't81crccertificacion'


class T82Comprobantesingreso(models.Model):
    t82id = models.AutoField(primary_key=True)
    t82fecha = models.DateField(blank=True, null=True)
    t82cliente = models.IntegerField(blank=True, null=True)
    t82formadepago = models.IntegerField(blank=True, null=True)
    t82detalle1 = models.TextField(blank=True, null=True)
    t82detalle2 = models.TextField(blank=True, null=True)
    t82detalle3 = models.TextField(blank=True, null=True)
    t82observaciones = models.TextField(blank=True, null=True)
    t82usuario = models.IntegerField(blank=True, null=True)
    t82recibide = models.IntegerField(blank=True, null=True)
    t82observacionanulado = models.TextField(blank=True, null=True)
    t82estado = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t82idanterior = models.CharField(max_length=100, blank=True, null=True)
    t82consecutivo = models.IntegerField(blank=True, null=True)
    t82total = models.FloatField(blank=True, null=True)
    t82sede = models.IntegerField(blank=True, null=True)
    t82anulado = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't82comprobantesingreso'


class T83Cuentas(models.Model):
    t83id = models.AutoField(primary_key=True)
    t83codigo = models.CharField(max_length=100, blank=True, null=True)
    t83descripcion = models.CharField(max_length=250, blank=True, null=True)
    t83nivel = models.CharField(max_length=10, blank=True, null=True)
    t83clase = models.CharField(max_length=10, blank=True, null=True)
    t83grupo = models.CharField(max_length=10, blank=True, null=True)
    t83subcuenta = models.CharField(max_length=10, blank=True, null=True)
    t83valor = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t83cuenta = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't83cuentas'


class T84Soportesarmas(models.Model):
    t84id = models.AutoField(primary_key=True)
    t84orden = models.IntegerField(blank=True, null=True)
    t84fecha = models.DateField(blank=True, null=True)
    t84usuario = models.IntegerField(blank=True, null=True)
    t84resultado = models.CharField(max_length=100, blank=True, null=True)
    t84observaciones = models.TextField(blank=True, null=True)
    t84numerocontrol = models.FloatField(blank=True, null=True)
    t84finalizado = models.DateField(blank=True, null=True)
    t84mental = models.NullBooleanField()
    t84psico = models.NullBooleanField()
    t84visual = models.NullBooleanField()
    t84audio = models.NullBooleanField()
    t84medico = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't84soportesarmas'


class T85Solicitudesmodificacion(models.Model):
    t85id = models.AutoField(primary_key=True)
    t85usuario = models.IntegerField(blank=True, null=True)
    t85fecha = models.DateField(blank=True, null=True)
    t85orden = models.IntegerField(blank=True, null=True)
    t85observacion = models.TextField(blank=True, null=True)
    t85autorizo = models.IntegerField(blank=True, null=True)
    t85autorizada = models.NullBooleanField()
    t85observacionautorizada = models.TextField(blank=True, null=True)
    t85sede = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't85solicitudesmodificacion'


class T86Formulas(models.Model):
    t86id = models.AutoField(primary_key=True)
    t86orden = models.IntegerField(blank=True, null=True)
    t86usuario = models.IntegerField(blank=True, null=True)
    t86fecha = models.DateField(blank=True, null=True)
    t86formula = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't86formulas'


class T87Certificadosrednacional(models.Model):
    t87id = models.AutoField(primary_key=True)
    t87orden = models.IntegerField(blank=True, null=True)
    t87usuario = models.IntegerField(blank=True, null=True)
    t87fecha = models.DateField(blank=True, null=True)
    t87fechacertificado = models.DateField(blank=True, null=True)
    t87horacertificado = models.TimeField(blank=True, null=True)
    t87examenes = models.TextField(blank=True, null=True)  # This field type is a guess.
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t87observaciones = models.TextField(blank=True, null=True)
    t87preguntas = models.TextField(blank=True, null=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 't87certificadosrednacional'


class T88Asistentes(models.Model):
    t88id = models.AutoField(primary_key=True)
    t88invitacion = models.IntegerField(blank=True, null=True)
    t88nit = models.CharField(max_length=30 ,blank=True, null=True)
    t88razonsocial = models.TextField(blank=True, null=True)
    t88telefono = models.CharField(max_length=30 ,blank=True, null=True)
    t88email = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t88evento = models.IntegerField(blank=True, null=True)
    t88usuario = models.IntegerField(blank=True, null=True)
    t88cargo = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't88asistentes'


class T89Profesiogramas(models.Model):
    t89id = models.AutoField(primary_key=True)
    t89cliente = models.IntegerField(blank=True, null=True)
    t89cargo = models.CharField(max_length=100, blank=True, null=True)
    t89sexo = models.CharField(max_length=1, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t89observacion = models.TextField(blank=True, null=True)
    t89tipo = models.CharField(max_length=100, blank=True, null=True)
    t89area = models.TextField(blank=True, null=True)
    t89unidad = models.IntegerField(blank=True, null=True)
    t89centrodecosto = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't89profesiogramas'


class T90Detalleprofesiograma(models.Model):
    t90id = models.AutoField(primary_key=True)
    t90profesiograma = models.IntegerField(blank=True, null=True)
    t90servicio = models.IntegerField(blank=True, null=True)
    t90ingreso = models.NullBooleanField()
    t90periodico = models.NullBooleanField()
    t90egreso = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t90opcional = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 't90detalleprofesiograma'


class T91Notificacionescartera(models.Model):
    t91id = models.AutoField(primary_key=True)
    t91cliente = models.IntegerField(blank=True, null=True)
    t91fecha = models.DateField(blank=True, null=True)
    t91observacion = models.TextField(blank=True, null=True)
    t91corte = models.DateField(blank=True, null=True)
    t91contacto = models.TextField(blank=True, null=True)
    t91telefono = models.TextField(blank=True, null=True)
    t91email = models.TextField(blank=True, null=True)
    t91cargo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t91usuario = models.IntegerField(blank=True, null=True)
    t91desde = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't91notificacionescartera'


class T92Seguimientoscartera(models.Model):
    t92id = models.AutoField(primary_key=True)
    t92notificacion = models.IntegerField(blank=True, null=True)
    t92usuario = models.IntegerField(blank=True, null=True)
    t92fecha = models.DateField(blank=True, null=True)
    t92hora = models.TimeField(blank=True, null=True)
    t92detalle = models.TextField(blank=True, null=True)
    t92telefono = models.TextField(blank=True, null=True)
    t92contacto = models.TextField(blank=True, null=True)
    t92cargo = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t92fechaproxima = models.DateField(blank=True, null=True)
    t92email = models.TextField(blank=True, null=True)
    t92horaproxima = models.TimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't92seguimientoscartera'


class T93Programacionesfacturacion(models.Model):
    t93id = models.AutoField(primary_key=True)
    t93desde = models.DateField(blank=True, null=True)
    t93hasta = models.DateField(blank=True, null=True)
    t93fecha = models.DateField(blank=True, null=True)
    t93usuario = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t93desdec = models.DateField(blank=True, null=True)
    t93hastac = models.DateField(blank=True, null=True)
    t93encargado = models.IntegerField(blank=True, null=True)
    t93cantidad = models.IntegerField(blank=True, null=True)
    t93cerrada = models.NullBooleanField()
    t93fechacierre = models.DateField(blank=True, null=True)
    t93fechaminima = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't93programacionesfacturacion'


class T94Detalleprogramacion(models.Model):
    t94id = models.AutoField(primary_key=True)
    t94cliente = models.IntegerField(blank=True, null=True)
    t94cantidadcomprobantes = models.IntegerField(blank=True, null=True)
    t94promedioexamenes = models.FloatField(blank=True, null=True)
    t94tiempo = models.FloatField(blank=True, null=True)
    t94usuario = models.IntegerField(blank=True, null=True)
    t94fecha = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t94programacion = models.IntegerField(blank=True, null=True)
    t94valortotal = models.FloatField(blank=True, null=True)
    t94comprobantes = models.TextField(blank=True, null=True)  # This field type is a guess.
    t94cumplida = models.NullBooleanField()
    t94factura = models.IntegerField(blank=True, null=True)
    t94activa = models.NullBooleanField()
    t94fechaminima = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't94detalleprogramacion'


class T95Valoracionfisioterapeuta(models.Model):
    t95id = models.AutoField(primary_key=True)
    t95fecha = models.DateTimeField(blank=True, null=True)
    t95usuario = models.IntegerField(blank=True, null=True)
    t95orden = models.IntegerField(blank=True, null=True)
    t95vistaanterior = models.TextField(blank=True, null=True)
    t95vistaposterior = models.TextField(blank=True, null=True)
    t95vistalateral = models.TextField(blank=True, null=True)
    t95observaciones = models.TextField(blank=True, null=True)
    t95jobederecha = models.TextField(blank=True, null=True)
    t95jobeizquierda = models.TextField(blank=True, null=True)
    t95yergasonderecha = models.TextField(blank=True, null=True)
    t95yergasonizquierda = models.TextField(blank=True, null=True)
    t95phalenderecha = models.TextField(blank=True, null=True)
    t95phalenizquierda = models.TextField(blank=True, null=True)
    t95filkesteinderecha = models.TextField(blank=True, null=True)
    t95filkesteinizquierda = models.TextField(blank=True, null=True)
    t95lateralderecha = models.TextField(blank=True, null=True)
    t95lateralizquierda = models.TextField(blank=True, null=True)
    t95medialderecha = models.TextField(blank=True, null=True)
    t95medializquierda = models.TextField(blank=True, null=True)
    t95thomasderecha = models.TextField(blank=True, null=True)
    t95thomasizquierda = models.TextField(blank=True, null=True)
    t95lassaguederecha = models.TextField(blank=True, null=True)
    t95lassagueizquierda = models.TextField(blank=True, null=True)
    t95anteriorderecha = models.TextField(blank=True, null=True)
    t95anteriorizquierda = models.TextField(blank=True, null=True)
    t95posteriorderecha = models.TextField(blank=True, null=True)
    t95posteriorizquierda = models.TextField(blank=True, null=True)
    t95hombroderecho = models.TextField(blank=True, null=True)
    t95hombroizquierda = models.TextField(blank=True, null=True)
    t95brazoderecha = models.TextField(blank=True, null=True)
    t95brazoizquierda = models.TextField(blank=True, null=True)
    t95cododerecha = models.TextField(blank=True, null=True)
    t95codoizquierda = models.TextField(blank=True, null=True)
    t95antebrazoderecha = models.TextField(blank=True, null=True)
    t95antebrazoizquierda = models.TextField(blank=True, null=True)
    t95manoderecha = models.TextField(blank=True, null=True)
    t95manoizquierda = models.TextField(blank=True, null=True)
    t95caderaderecha = models.TextField(blank=True, null=True)
    t95caderaizquierda = models.TextField(blank=True, null=True)
    t95rodilladerecha = models.TextField(blank=True, null=True)
    t95rodillaizquierda = models.TextField(blank=True, null=True)
    t95piernaderecha = models.TextField(blank=True, null=True)
    t95piernaizquierda = models.TextField(blank=True, null=True)
    t95piederecha = models.TextField(blank=True, null=True)
    t95pieizquierda = models.TextField(blank=True, null=True)
    t95observacionesfuerza = models.TextField(blank=True, null=True)
    t95sensibilidadsuperficial = models.TextField(blank=True, null=True)
    t95sensibilidadprofunda = models.TextField(blank=True, null=True)
    t95marcha = models.TextField(blank=True, null=True)
    t95observacionesmarcha = models.TextField(blank=True, null=True)
    t95artralgia = models.TextField(blank=True, null=True)
    t95localizacionartralgia = models.TextField(blank=True, null=True)
    t95inflamacion = models.TextField(blank=True, null=True)
    t95localizacioninflamacion = models.TextField(blank=True, null=True)
    t95edema = models.TextField(blank=True, null=True)
    t95localizacionedema = models.TextField(blank=True, null=True)
    t95diagnostico = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    t95estado = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't95valoracionfisioterapeuta'


class T96Costosipsaliada(models.Model):
    t96id = models.AutoField(primary_key=True)
    t96ips = models.IntegerField(blank=True, null=True)
    t96servicio = models.IntegerField(blank=True, null=True)
    t96costo = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't96costosipsaliada'


class T97Centrosdecosto(models.Model):
    t97id = models.AutoField(primary_key=True)
    t97cliente = models.IntegerField(blank=True, null=True)
    t97codigo = models.CharField(max_length=50, blank=True, null=True)
    t97nombre = models.CharField(max_length=100, blank=True, null=True)
    t97activo = models.NullBooleanField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't97centrosdecosto'


class T98Unidadesgestion(models.Model):
    t98id = models.AutoField(primary_key=True)
    t98cliente = models.IntegerField(blank=True, null=True)
    t98codigo = models.CharField(max_length=50, blank=True, null=True)
    t98nombre = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't98unidadesgestion'


class T99Restriccionesprofesiograma(models.Model):
    t99id = models.AutoField(primary_key=True)
    t99profesiograma = models.IntegerField(blank=True, null=True)
    t99restriccion = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 't99restriccionesprofesiograma'
