# encoding: utf-8
class respuestas_formaA():
	RESPUESTAS_TIPO_1=['SIEMPRE','CASI SIEMPRE','ALGUNAS VECES','CASI NUNCA','NUNCA']
	RESPUESTAS_TIPO_2=['SIEMPRE','CASI SIEMPRE','ALGUNAS VECES','CASI NUNCA','NUNCA']

	NUMERO_RESUESTAS_TIPO_1={'SIEMPRE':0,'CASI SIEMPRE':1,'ALGUNAS VECES':2,'CASI NUNCA':3,'NUNCA':4}
	NUMERO_RESUESTAS_TIPO_2={'SIEMPRE':4,'CASI SIEMPRE':3,'ALGUNAS VECES':2,'CASI NUNCA':1,'NUNCA':0}

	PREGUNTAS_TIPO_1=[4,5,6,9,12,14,32,33,34,39,40,41,42,43,44,45,46,47,48,49,50,
		51,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,
		76,77,78,79,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,
		102,103,104,105]

	PREGUNTAS_TIPO_2=[1,2,3,7,8,10,11,13,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,35,36,37,38
	,52,80,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123]

	def CalificacionClaseFormaA(self,numero_pregunta,opcion_respuesta):
		if(numero_pregunta in self.PREGUNTAS_TIPO_1):
			return([self.RESPUESTAS_TIPO_1[opcion_respuesta],self.NUMERO_RESUESTAS_TIPO_1[self.RESPUESTAS_TIPO_1[opcion_respuesta]]])
		else:
			return([self.RESPUESTAS_TIPO_2[opcion_respuesta],self.NUMERO_RESUESTAS_TIPO_2[self.RESPUESTAS_TIPO_2[opcion_respuesta]]])
				
		return(0)			
			
class respuestas_formaEstres():
	RESPUESTAS_TIPO_1=['SIEMPRE','CASI SIEMPRE','A VECES','NUNCA']
	NUMERO_RESUESTAS_TIPO_1={'SIEMPRE':9,'CASI SIEMPRE':6,'A VECES':3,'NUNCA':0}
	NUMERO_RESUESTAS_TIPO_2={'SIEMPRE':9,'CASI SIEMPRE':6,'A VECES':3,'NUNCA':0}
	NUMERO_RESUESTAS_TIPO_3={'SIEMPRE':9,'CASI SIEMPRE':6,'A VECES':3,'NUNCA':0}

	PREGUNTAS_TIPO_1=[1,2,3,9,13,14,15,23,24]
	PREGUNTAS_TIPO_2=[4,5,6,10,11,16,17,18,19,25,26,27,28]
	PREGUNTAS_TIPO_3=[7,8,12,20,21,22,29,30,31]

	def CalificacionClaseEstres(self,numero_pregunta,opcion_respuesta):

		if(numero_pregunta in self.PREGUNTAS_TIPO_1):
			return([self.RESPUESTAS_TIPO_1[opcion_respuesta],self.NUMERO_RESUESTAS_TIPO_1[self.RESPUESTAS_TIPO_1[opcion_respuesta]]])

		if(numero_pregunta in self.PREGUNTAS_TIPO_2):
			return([self.RESPUESTAS_TIPO_1[opcion_respuesta],self.NUMERO_RESUESTAS_TIPO_2[self.RESPUESTAS_TIPO_1[opcion_respuesta]]])

		if(numero_pregunta in self.PREGUNTAS_TIPO_3):
			return([self.RESPUESTAS_TIPO_1[opcion_respuesta],self.NUMERO_RESUESTAS_TIPO_3[self.RESPUESTAS_TIPO_1[opcion_respuesta]]])

		return(0)

class respuestas_formaExtralaboral():
	RESPUESTAS_TIPO_1=['SIEMPRE','CASI SIEMPRE','ALGUNAS VECES','CASI NUNCA','NUNCA']
	NUMERO_RESUESTAS_TIPO_1={'SIEMPRE':0,'CASI SIEMPRE':1,'ALGUNAS VECES':2,'CASI NUNCA':3,'NUNCA':4}
	NUMERO_RESUESTAS_TIPO_2={'SIEMPRE':4,'CASI SIEMPRE':3,'ALGUNAS VECES':2,'CASI NUNCA':1,'NUNCA':0}

	PREGUNTAS_TIPO_1=[1,4,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,25,27,29]
	PREGUNTAS_TIPO_2=[2,3,6,24,26,28,30,31]

	def CalificacionClaseExtralaborales(self,numero_pregunta,opcion_respuesta):
		
		if(numero_pregunta in self.PREGUNTAS_TIPO_1):
			return([self.RESPUESTAS_TIPO_1[opcion_respuesta],self.NUMERO_RESUESTAS_TIPO_1[self.RESPUESTAS_TIPO_1[opcion_respuesta]]])
		else:
			return([self.RESPUESTAS_TIPO_1[opcion_respuesta],self.NUMERO_RESUESTAS_TIPO_2[self.RESPUESTAS_TIPO_1[opcion_respuesta]]])
				
		return(0)	

class respuestas_socioDemografico_estudios():
	
	RESPUESTAS=['NINGUNO','BACHILLERATO INCOMPLETO','TECNICO/TECNOLOGO COMPLETO','CARRERA MILITAR / POLICIA','PRIMARIA INCOMPLETA','BACHILLERATO COMPLETO','PROFESIONAL INCOMPLETO','POST-GRADO INCOMPLETO','PRIMARIA COMPLETA','TECNICO/TECNOLOGO INCOMPLETO','PROFESIONAL COMPLETO','POST-GRADO COMPLETO']
	
	def nivel_de_estudios(self,opcion_respuesta):
	
		return(self.RESPUESTAS[opcion_respuesta])

class respuestas_socioDemografico_cedula():
	
	INDICES_VERTICALES_FILA_1=[0,13,26,39,52,65,78,91,104,117]
	INDICES_VERTICALES_FILA_2=[1,14,27,40,53,66,79,92,105,118]
	INDICES_VERTICALES_FILA_3=[2,15,28,41,54,67,80,93,106,119]
	INDICES_VERTICALES_FILA_4=[3,16,29,42,55,68,81,94,107,120]
	INDICES_VERTICALES_FILA_5=[4,17,30,43,56,69,82,95,108,121]
	INDICES_VERTICALES_FILA_6=[5,18,31,44,57,70,83,96,109,122]
	INDICES_VERTICALES_FILA_7=[6,19,32,45,58,71,84,97,110,123]
	INDICES_VERTICALES_FILA_8=[7,20,33,46,59,72,85,98,111,124]
	INDICES_VERTICALES_FILA_9=[8,21,34,47,60,73,86,99,112,125]
	INDICES_VERTICALES_FILA_10=[9,22,35,48,61,74,87,100,113,126]
	INDICES_VERTICALES_FILA_11=[10,23,36,49,62,75,88,101,114,127]
	INDICES_VERTICALES_FILA_12=[11,24,37,50,63,76,89,102,115,128]
	INDICES_VERTICALES_FILA_13=[12,25,38,51,64,77,90,103,116,129]
	index=0
	aux_fila_1=[]
	aux_fila_2=[]
	aux_fila_3=[]
	aux_fila_4=[]
	aux_fila_5=[]
	aux_fila_6=[]
	aux_fila_7=[]
	aux_fila_8=[]
	aux_fila_9=[]
	aux_fila_10=[]
	aux_fila_11=[]
	aux_fila_12=[]
	aux_fila_13=[]

	def cedula(self,calificaciones_parciales):
		self.INDICES_VERTICALES_FILA_1=[0,13,26,39,52,65,78,91,104,117]
		self.INDICES_VERTICALES_FILA_2=[1,14,27,40,53,66,79,92,105,118]
		self.INDICES_VERTICALES_FILA_3=[2,15,28,41,54,67,80,93,106,119]
		self.INDICES_VERTICALES_FILA_4=[3,16,29,42,55,68,81,94,107,120]
		self.INDICES_VERTICALES_FILA_5=[4,17,30,43,56,69,82,95,108,121]
		self.INDICES_VERTICALES_FILA_6=[5,18,31,44,57,70,83,96,109,122]
		self.INDICES_VERTICALES_FILA_7=[6,19,32,45,58,71,84,97,110,123]
		self.INDICES_VERTICALES_FILA_8=[7,20,33,46,59,72,85,98,111,124]
		self.INDICES_VERTICALES_FILA_9=[8,21,34,47,60,73,86,99,112,125]
		self.INDICES_VERTICALES_FILA_10=[9,22,35,48,61,74,87,100,113,126]
		self.INDICES_VERTICALES_FILA_11=[10,23,36,49,62,75,88,101,114,127]
		self.INDICES_VERTICALES_FILA_12=[11,24,37,50,63,76,89,102,115,128]
		self.INDICES_VERTICALES_FILA_13=[12,25,38,51,64,77,90,103,116,129]

		self.index=0
		self.aux_fila_1=[]
		self.aux_fila_2=[]
		self.aux_fila_3=[]
		self.aux_fila_4=[]
		self.aux_fila_5=[]
		self.aux_fila_6=[]
		self.aux_fila_7=[]
		self.aux_fila_8=[]
		self.aux_fila_9=[]
		self.aux_fila_10=[]
		self.aux_fila_11=[]
		self.aux_fila_12=[]
		self.aux_fila_13=[]
		cedula=""
		for i in self.INDICES_VERTICALES_FILA_1:
			self.aux_fila_1.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_1)>5000):
			cedula=str(self.aux_fila_1.index(min(self.aux_fila_1)))

		

		for i in self.INDICES_VERTICALES_FILA_2:
			self.aux_fila_2.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_2)>5000):

			cedula+=str(self.aux_fila_2.index(min(self.aux_fila_2)))

		

		for i in self.INDICES_VERTICALES_FILA_3:
			self.aux_fila_3.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_3)>5000):

			cedula+=str(self.aux_fila_3.index(min(self.aux_fila_3)))

		

		for i in self.INDICES_VERTICALES_FILA_4:
			self.aux_fila_4.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_4)>5000):

			cedula+=str(self.aux_fila_4.index(min(self.aux_fila_4)))

		

		for i in self.INDICES_VERTICALES_FILA_5:
			self.aux_fila_5.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_5)>5000):

			cedula+=str(self.aux_fila_5.index(min(self.aux_fila_5)))

		
		for i in self.INDICES_VERTICALES_FILA_6:
			self.aux_fila_6.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_6)>5000):

			cedula+=str(self.aux_fila_6.index(min(self.aux_fila_6)))

		for i in self.INDICES_VERTICALES_FILA_7:
			self.aux_fila_7.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_7)>5000):

			cedula+=str(self.aux_fila_7.index(min(self.aux_fila_7)))

		for i in self.INDICES_VERTICALES_FILA_8:
			self.aux_fila_8.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_8)>5000):

			cedula+=str(self.aux_fila_8.index(min(self.aux_fila_8)))

		for i in self.INDICES_VERTICALES_FILA_9:
			self.aux_fila_9.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_9)>5000):

			cedula+=str(self.aux_fila_9.index(min(self.aux_fila_9)))

		for i in self.INDICES_VERTICALES_FILA_10:
			self.aux_fila_10.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_10)>5000):

			cedula+=str(self.aux_fila_10.index(min(self.aux_fila_10)))

		

		for i in self.INDICES_VERTICALES_FILA_11:
			self.aux_fila_11.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_11)>5000):

			cedula+=str(self.aux_fila_11.index(min(self.aux_fila_11)))

		

		for i in self.INDICES_VERTICALES_FILA_12:
			self.aux_fila_12.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_12)>5000):

			cedula+=str(self.aux_fila_12.index(min(self.aux_fila_12)))

		

		for i in self.INDICES_VERTICALES_FILA_13:
			self.aux_fila_13.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_13)>5000):

			cedula+=str(self.aux_fila_13.index(min(self.aux_fila_13)))


		return(cedula)


class respuestas_socioDemografico_nacimiento():

	INDICES_VERTICALES_FILA_1=[0,4,8,12,16,20,24,28,32,36]
	INDICES_VERTICALES_FILA_2=[1,5,9,13,17,21,25,29,33,37]
	INDICES_VERTICALES_FILA_3=[2,6,10,14,18,22,26,30,34,38]
	INDICES_VERTICALES_FILA_4=[3,7,11,15,19,23,27,31,35,39]

	aux_fila_1=[]
	aux_fila_2=[]
	aux_fila_3=[]
	aux_fila_4=[]

	def nacimiento(self,calificaciones_parciales):

		nacimiento=""

		self.INDICES_VERTICALES_FILA_1=[0,4,8,12,16,20,24,28,32,36]
		self.INDICES_VERTICALES_FILA_2=[1,5,9,13,17,21,25,29,33,37]
		self.INDICES_VERTICALES_FILA_3=[2,6,10,14,18,22,26,30,34,38]
		self.INDICES_VERTICALES_FILA_4=[3,7,11,15,19,23,27,31,35,39]

		self.aux_fila_1=[]
		self.aux_fila_2=[]
		self.aux_fila_3=[]
		self.aux_fila_4=[]

		for i in self.INDICES_VERTICALES_FILA_1:
			self.aux_fila_1.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_1)>5000):
			nacimiento=str(self.aux_fila_1.index(min(self.aux_fila_1)))

		

		for i in self.INDICES_VERTICALES_FILA_2:
			self.aux_fila_2.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_2)>5000):

			nacimiento+=str(self.aux_fila_2.index(min(self.aux_fila_2)))

		

		for i in self.INDICES_VERTICALES_FILA_3:
			self.aux_fila_3.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_3)>5000):

			nacimiento+=str(self.aux_fila_3.index(min(self.aux_fila_3)))

		

		for i in self.INDICES_VERTICALES_FILA_4:
			self.aux_fila_4.append(calificaciones_parciales[i])
		if not(min(self.aux_fila_4)>5000):

			nacimiento+=str(self.aux_fila_4.index(min(self.aux_fila_4)))

		return(nacimiento)

class respuestas_socioDemografico_estado_civil():

	RESPUESTAS=["Soltero (a)","Casado (a)","Unión libre","Separado (a)","Divorciado (a)","Viudo (a)","Sacerdote / Monja"]

	def estado_civil(self,respuestas_parciales):
		opcion_respuesta=respuestas_parciales.index(min(respuestas_parciales))
		return(self.RESPUESTAS[opcion_respuesta])

class respuestas_socioDemografico_tipo_vivienda():
	RESPUESTAS=["PROPIA","EN ARRIENDO","FAMILIAR"]
	def vivienda(self,respuestas_parciales):
		opcion_respuesta=respuestas_parciales.index(min(respuestas_parciales))
		return(self.RESPUESTAS[opcion_respuesta])

class respuestas_socioDemografico_estrato():

	def estrato(self,respuestas_parciales):
		opcion_respuesta=respuestas_parciales.index(min(respuestas_parciales))
		RESPUESTAS=[1,4,'FINCA',2,5,'NO SE',3,6]
		return(RESPUESTAS[opcion_respuesta])

class respuestas_socioDemografico_sexo():
	
	def sexo(self,respuestas_parciales):
		opcion_respuesta=respuestas_parciales.index(min(respuestas_parciales))
		if(opcion_respuesta==0):
			return('MASCULINO')
		else:
			return('FEMENINO')

class respuestas_socioDemografico_dependientes():
	RESPUESTAS=[0,3,6,9,12,15,18,1,4,7,10,13,16,19,2,5,8,11,14,17,20]
	def dependientes(self,respuestas_parciales):

		return(self.RESPUESTAS[respuestas_parciales.index(min(respuestas_parciales))])

class respuestas_socioDemografico_duracion():
	
	def duracion(self,respuestas_parciales,menos_duracion):
		if(menos_duracion):
			return("MENOS DE UN AÑO")
		else:
			return(respuestas_parciales.index(min(respuestas_parciales))+1)
class respuestas_socioDemografico_tipo_cargo():
	RESPUESTAS=["JEFATURA-TIENE PERSONAL A CARGO","AUXILIAR,ASISTENTE ADMINISTRATIVO,ASISTENTE TECNICO","PROFESIONAL,ANALISTA,TECNICO,TECNOLOGO","OPERARIO,OPERADOR,AYUDANTE,SERVICIOS GENERALES"]
	def tipo_cargo(self,opcion_respuesta):
		index=opcion_respuesta.index(min(opcion_respuesta))
		return(self.RESPUESTAS[index])

class respuestas_socioDemografico_duracion_cargo():
	RESPUESTAS=[25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1]
	def duracion(self,opcion_respuesta,menos):
		if(menos):
			return("MENOS DE UN AÑO")
		else:
			return(self.RESPUESTAS[opcion_respuesta.index(min(opcion_respuesta))])
class respuestas_socioDemografico_tipo_de_contrato():
	RESPUESTAS=["TEMPORAL DE MENOS DE UN AÑO","TEMPORAL DE UN AÑO O MAS","TERMINO INDEFINIDO","COOPERADO(COOPERATIVA)","PRESTACION DE SERVICIOS","NO SE"]
	def tipo_contrato(self,opcion_respuesta):
		return(self.RESPUESTAS[opcion_respuesta.index(min(opcion_respuesta))])
class respuestas_socioDemografico_horas_diarias():
	RESPUESTAS=[24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1]

	def horas(self,opcion_respuesta):
		return(self.RESPUESTAS[opcion_respuesta.index(min(opcion_respuesta))])
class respuestas_sociodemografico_tipo_salario():
	RESPUESTAS=["FIJO","UNA PARTE FIJA OTRA VARIABLE","TODO VARIABLE"]
	def tipo(self,opcion_respuesta):
		return(self.RESPUESTAS[opcion_respuesta.index(min(opcion_respuesta))])