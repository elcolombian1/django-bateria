from django.contrib import admin
from django.conf.urls import url,include
from . import views


urlpatterns = [
	url(r'index',views.inicio,name="inicio"),	
    url(r'sociodemografico_2', views.sociodemografico_2 ,name="sociodemografico_2"),
    url(r'FormaA', views.formaA ,name="FormaA"),
    url(r'FormaB', views.formaA ,name="FormaB"),
    url(r'municipios/', views.get_municipios,name="municipios"),
    url(r'cargo/',views.add_cargo,name="cargo"),
    url(r'area/',views.add_area,name="area"),
    url(r'profesion/',views.add_profesion,name="profesion"),
]
